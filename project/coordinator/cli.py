import click
import json
import sys

from src import Daemon, settings, ConfigError


@click.group()
@click.option(
    '-c',
    '--config',
    help="Application configuration file",
    # default='/var/app-bin/rampdb.dev/project/coordinator/config/dev.ini',
    default='/var/www/faculty.dev/rampdb.dev/project/coordinator/config/dev.ini',
    type=click.Path(
        exists=True, file_okay=True, dir_okay=False, readable=True, resolve_path=True)
)
@click.pass_context
def cli(ctx, **options):
    try:
        settings.set_config(options['config'])
    except ConfigError as e:
        print('Critical:', str(e), file=sys.stderr)
        raise
    del options['config']
    settings.update({'cli': options})


@cli.command(name='start')
@click.option('-d', '--daemon', default=False, is_flag=True, help="Run as daemon.")
@click.pass_context
def start(ctx, **options):
    print(json.dumps(settings.config, sort_keys=True, indent=2))
    main = Daemon()
    if options.get('daemon', False):
        main.start()
    else:
        main.run()


@cli.command(name='stop')
@click.pass_context
def stop(ctx):
    Daemon().stop()


# DELETE TEST
@cli.command(name='test')
@click.pass_context
def test(ctx):
    from test.test import main
    main()


def main():
    cli(obj={})
