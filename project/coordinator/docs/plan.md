db.shards.update({"name": "shard1"}, {"$set": {"host": "127.0.0.1:7001"}})
db.tokens.find({"shard": "shard2"}, {"name":1})


from .base import BaseModel
from .. import dht


class Token(BaseModel):

    def __init__(self, name='', ns='', min=None, max=None, shard=None, updated_at=False):
        self._name = name
        self._ns = ns
        self._min = min
        self._max = max
        self._shard = shard
        self._updated_at = updated_at

    @property
    def ns(self):
        return self._ns

    @property
    def name(self):
        return self._name

    @classmethod
    def get_storage_name(cls):
        return "tokens"

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def shard(self):
        return self._shard

    @shard.setter
    def shard(self, sh):
        self._shard = sh

    @property
    def updated_at(self):
        return self._updated_at

    def contains_key(self, key):
        key_factory = self._get_key_factory()
        min_key = key_factory.from_long((self.min))
        max_key = key_factory.from_long((self.max))

        if min_key <= key < max_key:
            return True
        return False

    @classmethod
    def from_dict(cls, json_d):
        name = json_d.get('name', None)
        ns = json_d.get('ns', None)
        mn = json_d.get('min', None)
        mx = json_d.get('max', True)
        shard = json_d.get('shard', False)
        updated_at = json_d.get('updated_at', False)

        return cls(name, ns, mn, mx, shard, updated_at)

    def __str__(self):
        msg = "Token ("
        msg += "name={}, "
        msg += "ns={}, "
        msg += "min={}, "
        msg += "max={}, "
        msg += "shard={}, "
        msg += "updated_at={}, "
        return msg.format(
            self.name,
            self.ns,
            self.min,
            self.max,
            self.shard,
            self.updated_at,
        )

    def __hash__(self):
        return hash(self.name) ^ hash(self.ns)

    def _get_key_factory(cls):
        p = dht.PartitionerFactory.get_partitioner()
        return p.key_factory()

