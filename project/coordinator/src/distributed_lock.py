from .catalog import CatalogManagerFactory
from .model import Locks
from .dates import current_utc_date
import time
from .logger import get_main_logger


class LockScope(object):

    def __init__(self, tp=None, scope=None):
        self.__type = tp
        self.__scope = scope

    def is_cluster_scope(self):
        return self.__type == "cluster_scope"

    def is_collection_scope(self):
        return self.__type == "collection_scope"

    def is_database_scope(self):
        return self.__type == "database_scope"

    @property
    def scope(self):
        return self.__scope

    @classmethod
    def cluster_scope(cls):
        return cls("cluster_scope")

    @classmethod
    def collection_scope(cls, scope):
        return cls("collection_scope", scope)

    @classmethod
    def database_scope(cls, scope):
        return cls("database_scope", scope)

    @classmethod
    def from_text(cls, text):
        if text == "cluster":
            return cls.cluster_scope()

        parts = text.split(".")
        ln = len(parts)
        if ln > 2:
            raise ValueError("Bad lock scope resolution for: {}".format(text))
        if ln == 1:
            return cls.database_scope(text)

        if ln == 2:
            return cls.collection_scope(text)


class DistributedLock(object):
    __catalog_query = None

    def __init__(self):
        self.__catalog_query = CatalogManagerFactory.inst().get_catalog_query()
        self.__ident = None
        self.logger = get_main_logger()

    def busy_acquire(self, scope, who, why):
        msg = "{} is trying to get {} lock for {}".format(who, scope, why)
        self.logger.info(msg)
        if isinstance(scope, str):
            scope = LockScope.from_text(scope)

        doc = self.get_lock_doc(scope, who, why)
        while not self.can_be_locked(scope):
            self.logger.warning("lock busy")
            time.sleep(1)

        result = self.__catalog_query.insert(Locks.get_full_ns(), doc)

        self.check_result_status(result)

        # TODO check result we need to extract the id
        self.extract_insert_id(result.any())

        self.logger.info("lock acquired")

    def release(self):
        if self.__ident:
            result = self.__catalog_query.remove(
                Locks.get_full_ns(),
                {"_id": self.__ident}
            )

        self.check_result_status(result)

    def check_result_status(self, result):
        # if not result:
        # raise CoordinatorError("Empty lock response! Got: {}".format(result))
        if not result.status.is_ok():
            raise result.status.to_exception()

    def extract_insert_id(self, response):
        ids = response.get("ids", [])
        if isinstance(ids, list):
            self.__ident = ids[0]
            return

        msg = "Cannot find lock insert id! Response: {}".format(str(response))
        raise Exception(msg)

    def get_lock_doc(self, scope, who, why):
        d = {
            "scope": scope.scope,
            "who": who,
            "why": why,
            "when": current_utc_date()
        }

        return d

    def can_be_locked(self, lock_scope):
        if lock_scope.is_cluster_scope():
            q = {}
        else:
            q = {"scope": lock_scope.scope}

        result = self.__catalog_query.one_example(
            Locks.get_full_ns(),
            q
        )

        # TODO check doc count here
        if result:
            return False
        else:
            return True
