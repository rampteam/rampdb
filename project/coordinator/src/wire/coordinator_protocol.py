import logging
import traceback

from bson import BSON

from ..errors import InvalidCommandError, InvalidName
from ..config import settings
from .handlers import Collection, Database, Coordinator
from ..base import Status


class CoordinatorProtocol(object):

    def __init__(self):
        self._logger = logging.getLogger(settings.get('logger_name'))

    def data_received(self, data):
        data = BSON.decode(data)
        meta = data.get('c').split('.')
        if len(meta) != 2:
            raise InvalidCommandError(
                'Invalid command: {}'.format(data.get('c')))
        command = {
            'type': meta[0],
            'command': meta[1],
            'params': data.get('p'),
            # 'raw': data
        }

        try:
            response = {'ok': 1, 'data': self.handle(command)}
        except Exception as e:
            self._logger.warning(traceback.format_exc())
            response = {'ok': 0, 'error': str(e).encode()}
        return BSON.encode(response)

    def handle(self, command: dict):
        handler = getattr(self, 'handle_{}'.format(command['type']), None)
        if callable(handler):
            data = handler(command)
            if isinstance(data, Status):
                data = data.get_data()

            return data
        else:
            raise ValueError(
                "No handler found for {} {}".format(command['type'], command['command']))

    def get_collection_handler(self, ns):
        return Collection(ns)

    def get_database_handler(self, ns):
        return Database(ns)

    def get_coordinator_handler(self, ns):
        return Coordinator(ns)

    def handle_c(self, command: dict):
        ns = command['params'].get('ns')
        collection = self.get_collection_handler(ns)

        return self.delegate(collection, command)

    def handle_d(self, command):
        ns = command['params'].get('ns')

        database = self.get_database_handler(ns)

        return self.delegate(database, command)

    def handle_cs(self, command):
        ns = command['params'].get('ns')
        coord = self.get_coordinator_handler(ns)

        return self.delegate(coord, command)

    def delegate(self, to, command):

        cmd = getattr(to, command['command'], None)

        if not callable(cmd):
            raise InvalidCommandError(
                'Command {} is invalid'.format(command['command']))
        return cmd(command['params'])
