from ..commands import *


class Collection:

    def __init__(self, ns):
        self.ns = ns

    def insert(self, params: dict):
        cmd = InsertCmd(self.ns, params)
        return cmd.run()

    def query(self, params: dict):
        cmd = FindCmd(self.ns, params)
        return cmd.run()

    def remove(self, params):
        cmd = RemoveCmd(self.ns, params)
        return cmd.run()

    def update(self, params):
        cmd = UpdateCmd(self.ns, params)
        return cmd.run()

    def rename_collection(self, params: dict):
        cmd = RenameCollectionCmd(self.ns, params)
        return cmd.run()

    def drop_index(self, params: dict):
        cmd = IndexOpCmd(self.ns, params)
        return cmd.drop_index()

    def create_index(self, params: dict):
        cmd = IndexOpCmd(self.ns, params)
        return cmd.create_index()


class Database:

    def __init__(self, ns):
        self.ns = ns

    def collection_names(self, params: dict):
        cmd = CollectionNamesCmd(self.ns, params)
        return cmd.run()

    def drop_collection(self, params: dict):
        cmd = DropCreateCollectionCmd(self.ns, params)
        return cmd.drop_collection()

    def create_collection(self, params: dict):
        cmd = DropCreateCollectionCmd(self.ns, params)
        return cmd.create_collection()

    def command(self, params: dict):
        cmd = DatabaseCustomCmd(self.ns, params)
        return cmd.run()


class Coordinator(object):

    def __init__(self, ns):
        self.ns = ns

    def enable_sharding(self, params: dict):
        cmd = EnableSharding(self.ns, params)
        return cmd.enable_sharding()

    def shard_collection(self, params):
        cmd = EnableSharding(self.ns, params)
        return cmd.shard_collection()

    def add_shard(self, params: dict):
        cmd = ShardCmd(self.ns, params)
        return cmd.add_shard()

    def remove_shard(self, params: dict):
        cmd = ShardCmd(self.ns, params)
        return cmd.remove_shard()
