from .config import settings
import logging
import sys

LOG_FORMAT = "[%(asctime)s]|%(levelname)s|%(module)s|%(threadName)s: %(message)s"
LOGGER = None


def get_main_logger():
    global LOGGER

    if LOGGER is None:
        set_logging(
            settings.get('log_level', 'INFO').upper(),
            settings.get('logger_name')
        )

    return LOGGER


def set_logging(log_level, logger_name):
    global LOGGER

    logger = logging.getLogger(logger_name)
    logger = logger
    logger.setLevel(log_level)

    handler = logging.StreamHandler(stream=sys.stderr)
    handler.setLevel(log_level)
    handler.setFormatter(logging.Formatter(LOG_FORMAT))

    logger.addHandler(handler)

    LOGGER = logger
