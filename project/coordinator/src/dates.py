# coding: utf-8
from __future__ import absolute_import
import dateutil.tz
from datetime import datetime
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import time
import pytz


def get_utc_date(dt, tz_name="UTC"):
    """
    Given a dt argument will try to parse and return
    an utc aware datetime object

    """
    dt = get_datetime_obj(dt)

    if dt.tzinfo is None:
        #not aware
        return naive_to_utc(dt, tz_name)
    else:
        # aware
        return local_to_utc(dt)


def local_to_utc(dt):
    """Converts TZ-aware (with tzinfo) datetime object to UTC time.
    """
    if dt.tzinfo is None:
        raise ValueError(
            u"dt argument MUST be TZ-aware datetime (with tzinfo).")
    return pytz.utc.normalize(dt.astimezone(pytz.utc))


def naive_to_local(dt, tz_name):
    """Shortcut for tz = pytz.timezone(tz_name); tz.normalize(tz.localize(dt)).
    """
    tz = pytz.timezone(tz_name)
    return tz.normalize(tz.localize(dt))


def naive_to_utc(dt, tz_name):
    """Converts naive (w/o tzinfo) datetime object to UTC time.

    `tz_name` must be a symbolic name of time zone of `dt`
    , e.g. 'Europe/Moscow'.
    Use `os.environ['TZ']` if unsure.

    Shortcut for `naive_to_local(dt, tz_name).astimezone(pytz.utc)`
     with `dt.tzinfo` check.
    """
    if dt.tzinfo is not None:
        raise ValueError(u"dt argument MUST be naive datetime (w/o tzinfo).")
    return naive_to_local(dt, tz_name).astimezone(pytz.utc)


def utc_to_local(dt, tz_name):
    """Converts TZ-aware UTC datetime to local for given time zone.

    `tz_name` must be a symbolic name of time zone
    of `dt`, e.g. 'Europe/Moscow'.
    Use `os.environ['TZ']` if unsure.
    """
    if dt.tzinfo is not pytz.utc:
        raise ValueError(u"dt argument MUST be TZ-aware UTC datetime \
    To convert naive UTC datetime, use dt.replace(tzinfo=pytz.utc)")
    assert dt.tzinfo is pytz.utc
    tz = pytz.timezone(tz_name)
    return dt.astimezone(tz)


def now_as_local(tz_name):
    """
    Shortcut for `naive_to_local(datetime.datetime.now, tz_name)`.

    """
    return naive_to_local(datetime.now(), tz_name)


def now_as_utc(tz_name):
    """
    Shortcut for `naive_to_utc(datetime.now(), tz_name)`.

    """
    return naive_to_utc(datetime.now(), tz_name)


def yesterday_utc_date(tz_name="UTC"):
    """
    Returns utc date for the current time - 24h

    """

    return current_utc_date() + relativedelta(days=-1)


def trunc_time(dt):
    """
    Truncates time info on a given date

    """
    dt = get_datetime_obj(dt)
    return dt.replace(hour=0, minute=0, second=0, microsecond=0)


def is_utc_today(dt):
    """
    Checks if given date rep is utc today

    """

    dt = get_utc_date(dt)
    dt = dt.replace(hour=0, minute=0, second=0, microsecond=0)
    current_dt = current_utc_date()
    current_dt = current_dt.replace(hour=0, minute=0, second=0, microsecond=0)
    return current_dt == dt


def current_utc_date():
    """
    CURRENT AWARE DATETIME IN UTC TIMEZONE

    """
    utc_tz = pytz.timezone('UTC')
    utc_dt = datetime.now(utc_tz)
    return utc_dt


def current_utc_date_midnight():
    current_utc = current_utc_date()
    return trunc_time(current_utc)


def datetime_to_timestamp(datetime_ob):
    return time.mktime(datetime_ob.timetuple())


def datetime_to_str(datetime_ob, fmt="%Y-%m-%d %H:%M:%S"):
    return datetime_ob.strftime(fmt)


def datetime_from_timestamp(timestamp):
    return datetime.datetime.fromtimestamp(timestamp)


def from_tmz_to_utc(dt, tz_name):
    """
    What is the UTC time given the local time and the time zone's name?

    """
    if not isinstance(dt, datetime):
        dt = parse(dt)

    dt = pytz.timezone(tz_name).localize(dt)
    utc_dt = pytz.utc.normalize(dt.astimezone(pytz.utc))
    return utc_dt.isoformat()


def from_utc_to_tmz(dt, tz_name):
    """
    What is the local time in the user-provided time zone name?

    """
    if not isinstance(dt, datetime):
        dt = parse(dt)

    local_tz = pytz.timezone(tz_name)
    dt = local_tz.normalize(dt.astimezone(local_tz))
    return dt


def get_utc_from_string(dt):
    """
    Parse the given string and return an UTC date time object

    """
    tz_infos = {
        "UTC": dateutil.tz.tzutc()
    }

    dt = parse(dt, tzinfos=tz_infos)
    return dt


def get_datetime_obj(dt):
    """
    Returns a datetime obj

    """
    if isinstance(dt, datetime):
        return dt
    # if string parse it into a datetime
    if isinstance(dt, str):
        return parse(dt)

    raise ValueError(u"Could not convert" + str(dt) + "to datetime!")


def timestamp_to_utc(timestamp):
    """
    Timestamp to utc aware datetime

    """
    utc_tz = pytz.timezone('UTC')
    utc_dt = utc_tz.localize(datetime.utcfromtimestamp(timestamp))
    return utc_dt


def current_timestamp():
    """
    Current timestamp

    """
    return int(time())
