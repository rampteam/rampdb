from .dht import PartitionerFactory
from .collection_metadata import CollectionMetadata
from .shards import ShardInfo
from .token_metadata import TokenMetadata
from .token_manager import TokenManager


class CoordinatorFactory(object):

    def __init__(self, arg):
        super(CoordinatorFactory, self).__init__()
        self.arg = arg

    @classmethod
    def get_key_factory(cls):
        p = PartitionerFactory.get_partitioner()
        return p.key_factory()

    @classmethod
    def get_collection_metadata(cls):
        return CollectionMetadata.instance()

    @classmethod
    def get_token_metadata(cls, ns):
        return TokenMetadata(ns)

    @classmethod
    def get_token_manager(cls):
        return TokenManager.instance()

    @classmethod
    def get_shard_info(cls):
        return ShardInfo.instance()
