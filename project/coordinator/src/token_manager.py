from .shards import BaseShard, ShardInfo
from .model import Token
from .dbclient import Connection
from .base import Status
from .migrate import MoveTokenCommand


class TokenManager(object):

    __instance = None

    @classmethod
    def instance(cls):
        if not cls.__instance:
            cls.__instance = cls()
        return cls.__instance

    def move_token_op(self, data):
        # TODO this should be in some kind of transaction
        target_node = data['target']
        shard_name = target_node
        op = data['op']
        op_data = data['data']
        meta = data.get("meta", {})
        self._current_meta = meta
        if op == "set":
            # nothing to move
            return Status.OK()

        if op == "move_to_set":
            return self.move_to_shard_tokens(op_data, shard_name)

        if op == "move_to_remove":
            return self.move_to_shards_tokens(op_data, shard_name)

        raise ValueError("Unknown token op : {}!".format(op))

    def check_token_op(self, status):
        fail_msg = "unable to move token Err: {}"
        if not status.is_ok():
            return Status(status.code(),
                          fail_msg.format(str(status))
                          )
        return Status.OK()

    def move_to_shard_tokens(self, op_data, target_shard_name):
        for sh, tokens in op_data.items():
            for token in tokens:
                response = self.move_and_commit(token, target_shard_name)
                last_status = self.check_token_op(response)
                if not last_status.is_ok():
                    return last_status

        return Status.OK()

    def move_to_shards_tokens(self, op_data, source_shard_name):
        for sh, tokens in op_data.items():
            for token in tokens:
                response = self.move_and_commit(token, sh)
                last_status = self.check_token_op(response)
                if not last_status.is_ok():
                    return last_status

        return Status.OK()

    def move_and_commit(self,
                        token: Token,
                        to_shard_name,
                        wait_delete=True
                        ):

        names_to_host = self._current_meta["sh_names_to_host"]
        from_shard = names_to_host[token.shard]
        to_shard = names_to_host[to_shard_name]

        options = {
            "full_ns": token.ns,
            "from_shard": from_shard,
            "to_shard": to_shard,
            "min": token.min,
            "max": token.max,
            "wait_for_delete": wait_delete
        }

        mv = MoveTokenCommand(options)

        status = mv.run()
        return status

    def move_and_commit_cmd(self,
                            token: Token,
                            to: BaseShard,
                            wait_delete=True
                            ):
        ns = token.ns
        # create query
        p = {
            "name": "move_chunk",
            "full_ns": ns,
            "from_shard": token.conn_string.to_string,
            "to_shard": to,
            "min": token.min,
            "max": token.max,
            "wait_for_delete": wait_delete
        }

        from_conn = Connection.from_conn_string(token.conn_string)

        response = from_conn.execute_cmd("run_command", "admin", p)
        # TODO parse response
        return response
