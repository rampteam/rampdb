from queue import Queue
from contextlib import contextmanager


class ObjectPool:

    def __init__(self, object_fn, *args, **kwargs):
        self.object_fn = object_fn
        self.args = args
        self.max_size = int(kwargs.get("max_size", 1))
        self.queue = Queue()

    def _get_obj(self):
        return self.object_fn(*self.args)

    def borrow_obj(self):
        if self.queue.qsize() < self.max_size and self.queue.empty():
            self.queue.put(self._get_obj())
        return self.queue.get()

    def return_obj(self, obj):
        self.queue.put(obj)


@contextmanager
def acquire_object(pool):
    obj = pool.borrow_obj()
    try:
        yield obj
    finally:
        pool.return_obj(obj)
