from .catalog import CatalogManagerFactory
from .shards import ShardInfo
import copy


class TokenMetadata(object):

    __instance = None

    def __init__(self, ns):
        super(TokenMetadata, self).__init__()
        self._tokens_to_shards = {}
        self._shards_to_tokens = {}
        self._name_to_tokens = {}
        self._ns = ns
        self.reload()

    @property
    def ns(self):
        return self._ns

    @classmethod
    def instance(cls):
        if not cls.__instance:
            cls.__instance = cls()
        return cls.__instance

    def reload(self):
        catalog = self._get_catalog_data()
        token_list = catalog.get_token_models(self.ns)

        for token_model in token_list:
            sh = token_model.shard
            self._tokens_to_shards[
                token_model.name] = sh

            self._name_to_tokens[token_model.name] = token_model

            if sh not in self._shards_to_tokens:
                self._shards_to_tokens[sh] = []

            self._shards_to_tokens[sh].append(token_model)

    @property
    def shards_to_tokens(self):
        return copy.deepcopy(self._shards_to_tokens)

    @property
    def tokens_to_shards(self):
        return copy.deepcopy(self._tokens_to_shards)

    def get_tokens_for_shard(self, sh_name):
        if sh_name in self._shards_to_tokens:
            return self._shards_to_tokens[sh_name]

        return []

    def get_token_by_name(self, name):
        if name in self._name_to_tokens:
            return self._name_to_tokens[name]

    def get_shard_for_token(self, token):
        if token in self._tokens_to_shards:
            return self._tokens_to_shards[token]

    def get_all_tokens(self):
        return list(self._name_to_tokens.values())

    def get_token_for_key(self, key):
        # TODO Sort & speedup this process
        tokens = self.get_all_tokens()
        for token in tokens:
            if token.contains_key(key.key):
                # print(key.key)
                # print(token)
                return token

        return None

    def get_shard_for_key(self, key):
        token = self.get_token_for_key(key)
        if not token:
            return token

        name = self._tokens_to_shards[token.name]
        sh_info = self._get_shard_info()
        return sh_info.find_by_name_with_retry(name)

    def _get_catalog_data(self):
        return CatalogManagerFactory.inst().get_catalog_data()

    def _get_shard_info(self):
        return ShardInfo.instance()
