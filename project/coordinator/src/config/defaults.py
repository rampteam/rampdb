default_values = {
    'general': {
        'pid_file': '/tmp/coordinator.pid',
        'log_level': 'DEBUG',
        'error_log': '/dev/null',
        'logger_name': 'coordinator'
    }
}