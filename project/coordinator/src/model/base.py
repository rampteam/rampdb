from ..dates import current_utc_date


class BaseModel:

    @classmethod
    def get_database_name(cls):
        return "config"

    @classmethod
    def get_full_ns(cls):
        return "{}.{}".format(
            cls.get_database_name(),
            cls.get_storage_name()
        )

    def get_storage_name(self):
        return ""

    def props(self):
        d = {}
        for key, value in self.__dict__.items():
            if callable(value) or key.startswith('__'):
                continue

            uniform_key = key.strip('_')
            d[uniform_key] = value

        return d

    def to_dict(self):
        return self.props()

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def get_current_date(self):
        return current_utc_date()
