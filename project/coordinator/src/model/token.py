from .base import BaseModel


class Token(BaseModel):

    def __init__(self, name='', ns='', min=None, max=None, shard=None, updated_at=False):
        self._name = name
        self._ns = ns
        self._min = min
        self._max = max
        self._shard = shard
        self._updated_at = updated_at

    @property
    def ns(self):
        return self._ns

    @property
    def name(self):
        return self._name

    @classmethod
    def get_storage_name(cls):
        return "tokens"

    @property
    def min(self):
        return self._min

    @property
    def max(self):
        return self._max

    @property
    def shard(self):
        return self._shard

    @shard.setter
    def shard(self, sh):
        self._shard = sh

    @property
    def updated_at(self):
        return self._updated_at

    def contains_key(self, key):
        # print(self.min)
        # print(key)
        # print(self.max)
        # print("\n")

        return self.min <= key < self.max

    @classmethod
    def from_dict(cls, json_d):
        name = json_d.get('name', None)
        ns = json_d.get('ns', None)
        mn = json_d.get('min', None)
        mx = json_d.get('max', True)
        shard = json_d.get('shard', False)
        updated_at = json_d.get('updated_at', False)

        return cls(name, ns, mn, mx, shard, updated_at)

    def __str__(self):
        msg = "Token ("
        msg += "name={}, "
        msg += "ns={}, "
        msg += "min={}, "
        msg += "max={}, "
        msg += "shard={}, "
        msg += "updated_at={}, "
        return msg.format(
            self.name,
            self.ns,
            self.min,
            self.max,
            self.shard,
            self.updated_at,
        )

    def __repr__(self):
        return "Tk({})".format(self.name)

    def __hash__(self):
        return hash(self.name) ^ hash(self.ns)
