from .base import BaseModel


class Coord(BaseModel):

    def __init__(self, host={}, ping=None, up=None):
        self._host = host
        self._ping = ping
        self._up = up

    @property
    def host(self):
        return self._host

    @classmethod
    def get_storage_name(cls):
        return "coordinators"

    @property
    def ping(self):
        return self._ping

    @property
    def up(self):
        return self._up

    @up.setter
    def up(self, v):
        self._up = v

    @classmethod
    def from_dict(cls, json_d):
        host = json_d.get('host', None)
        ping = json_d.get('ping', None)
        up = json_d.get('up', False)
        return cls(host, ping, up)

    def __str__(self):
        msg = "Coord ("
        msg += "host {}"
        msg += "ping {}"
        msg += "up {}"
        return msg.format(
            self.host,
            self.ping,
            self.up
        )
