from .base import BaseModel


class ChangeLog(BaseModel):

    def __init__(self, server='', client_addr='', time=None, what='', ns='', details={}):
        self._server = server
        self._client_addr = client_addr
        self._what = what
        self._ns = ns
        self._details = details
        self._time = time
        if not self._time:
            self._time = self.get_current_date()

    @property
    def client_addr(self):
        return self._client_addr

    @property
    def server(self):
        return self._server

    @classmethod
    def get_storage_name(cls):
        return "changelog"

    @property
    def time(self):
        return self._time

    @property
    def what(self):
        return self._what

    @property
    def ns(self):
        return self._ns

    @ns.setter
    def ns(self, sh):
        self._ns = sh

    @property
    def details(self):
        return self._details

    @classmethod
    def from_dict(cls, json_d):
        server = json_d.get('server', None)
        client_addr = json_d.get('client_addr', None)
        time = json_d.get('time', None)
        what = json_d.get('what', True)
        ns = json_d.get('ns', False)
        details = json_d.get('details', False)

        return cls(server, client_addr, time, what, ns, details)

    def __str__(self):
        msg = "ChangeLog ("
        msg += "server={}, "
        msg += "client_addr={}, "
        msg += "time={}, "
        msg += "what={}, "
        msg += "ns={}, "
        msg += "details={}, "
        return msg.format(
            self.server,
            self.client_addr,
            self._time,
            self._what,
            self.ns,
            self.details,
        )

    def __repr__(self):
        return "Ch({})".format(self.server)

    def __hash__(self):
        return hash(self.server) ^ hash(self.client_addr)
