from .base import BaseModel


class Locks(BaseModel):

    def __init__(self, scope=None, lock_id=None, who=None, why=False, when=None):
        self._scope = scope
        self._lock_id = lock_id
        self._who = who
        self._why = why
        self._when = when

    @property
    def scope(self):
        return self._scope

    @classmethod
    def get_storage_name(cls):
        return "locks"

    @property
    def lock_id(self):
        return self._lock_id

    @property
    def who(self):
        return self._who

    @property
    def why(self):
        return self._why

    @property
    def when(self):
        return self._when

    @classmethod
    def from_dict(cls, json_d):
        scope = json_d.get('scope', None)
        addr = json_d.get('lock_id', None)
        who = json_d.get('who', True)
        why = json_d.get('why', False)
        when = json_d.get('when', False)

        return cls(scope, addr, who, why, when)

    def __str__(self):
        msg = "Locks ("
        msg += "scope {}"
        msg += "addr {}"
        msg += "who {}"
        msg += "why {}"
        msg += "when {}"
        return msg.format(
            self.scope,
            self.addr,
            self.who,
            self.why,
            self.when,
        )
