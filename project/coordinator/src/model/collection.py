from .base import BaseModel


class Collection(BaseModel):

    def __init__(self, full_name='', key={}, updated_at=None, unique=False, dropped=False):
        self._full_name = full_name
        self._key = key
        self._updated_at = updated_at
        self._unique = unique
        self._dropped = dropped

    @property
    def key(self):
        return self._key

    @property
    def full_name(self):
        return self._full_name

    @classmethod
    def get_storage_name(cls):
        return "collections"

    @property
    def updated_at(self):
        return self._updated_at

    @property
    def unique(self):
        return self._unique

    @property
    def dropped(self):
        return self._dropped

    @classmethod
    def from_dict(cls, json_d):
        full_name = json_d.get('full_name', None)
        key = json_d.get('key', None)
        updated_at = json_d.get('updated_at', None)
        unique = json_d.get('unique', False)
        dropped = json_d.get('dropped', False)

        return cls(full_name, key, updated_at, unique, dropped)

    def __str__(self):
        msg = "Collection ("
        msg += "full_name {}"
        msg += "key {}"
        msg += "updated_at {}"
        msg += "unique {}"
        msg += "dropped {}"
        return msg.format(
            self.full_name,
            self.key,
            self.updated_at,
            self.unique,
            self.dropped,
        )
