from .base import BaseModel


class Database(BaseModel):

    def __init__(self, name={}, primary=None, sharded=None):
        self._name = name
        self._primary = primary
        self._sharded = sharded

    @property
    def name(self):
        return self._name

    @classmethod
    def get_storage_name(cls):
        return "databases"

    @property
    def primary(self):
        return self._primary

    @property
    def sharded(self):
        return self._sharded

    @sharded.setter
    def sharded(self, v):
        self._sharded = v

    @classmethod
    def from_dict(cls, json_d):
        name = json_d.get('name', None)
        primary = json_d.get('primary', None)
        sharded = json_d.get('sharded', False)
        return cls(name, primary, sharded)

    def __str__(self):
        msg = "Database ("
        msg += "name {}"
        msg += "primary {}"
        msg += "sharded {}"
        return msg.format(
            self.name,
            self.primary,
            self.sharded
        )
