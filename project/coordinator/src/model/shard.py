from .base import BaseModel


class Shard(BaseModel):

    def __init__(self, name=None, host=None, max_size=-1, is_draining=False):
        self._name = name
        self._host = host
        self._max_size = max_size
        self._is_draining = is_draining

    @property
    def name(self):
        return self._name

    @classmethod
    def get_storage_name(cls):
        return "shards"

    @property
    def host(self):
        return self._host

    @property
    def max_size(self):
        return self._max_size

    @property
    def is_draining(self):
        return self._is_draining

    @classmethod
    def from_dict(cls, json_d):
        name = json_d.get('name', None)
        addr = json_d.get('host', None)
        max_size = json_d.get('max_size', -1)
        is_draining = json_d.get('draining', False)

        return cls(name, addr, max_size, is_draining)

    def __str__(self):
        msg = "Shard ("
        msg += "name={}, "
        msg += "addr={}, "
        msg += "max_size={}, "
        msg += "is_draininig={} )"
        return msg.format(
            self.name,
            self.host,
            self.max_size,
            self.is_draining,
        )

    def __hash__(self):
        return hash(self.name) ^ hash(self.host)
