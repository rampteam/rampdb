from .base_cmd import BaseCmd


class UpdateCmd(BaseCmd):

    def __init__(self, ns, options):
        super(UpdateCmd, self).__init__(ns, options)
        self._query = options.get("query", {})
        self._update = options.get("update", {})
        self._upsert = options.get("upsert", False)
        self._multi = options.get("multi", True)

    def run(self):
        shards = self._get_query_based_targeted_shards(self._query)

        all_results = []
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)
            result = coll.update(
                spec=self._query,
                document=self._update,
                multi=self._multi,
                upsert=self._upsert,
            )

            # TODO check result
            all_results.append(result)

        return self.aggregate(all_results)

    def aggregate(self, results):
        data = {
            "nMatched": 1,
            "nUpserted": 0,
            "nModified": 1
        }

        for result in results:
            for elem, val in result.items():
                if elem in data:
                    data[elem] += val

        return data
