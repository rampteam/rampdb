from .base_cmd import BaseCmd


class RemoveCmd(BaseCmd):

    def __init__(self, ns, options):
        super(RemoveCmd, self).__init__(ns, options)
        self._query = options.get("spec", {})
        self._single = options.get("single", False)

    def run(self):
        shards = self.get_all_shards()
        all_results = []
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)
            result = coll.remove(
                spec=self._query,
                single=self._single,
            )

            # TODO check result
            all_results.append(result)

        return self.aggregate(all_results)

    def aggregate(self, results):
        data = {
            "n": 0,
        }

        for result in results:
            for elem, val in result.items():
                if elem in data:
                    data[elem] += val

        return data
