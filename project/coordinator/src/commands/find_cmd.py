from .base_cmd import BaseCmd


class FindCmd(BaseCmd):

    def __init__(self, ns, options={}):
        super(FindCmd, self).__init__(ns, options)
        self._query = options.get("query", {})
        self._skip = options.get("n_to_skip", 0)
        self._limit = options.get("n_to_return", 0)
        self._fields = options.get("fields", None)
        self._ns = options.get("ns", None)

    def run(self):
        shards = self._get_query_based_targeted_shards(self._query)
        final_results = []
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)

            results = coll.by_example(
                spec=self._query,
                fields=self._fields,
            )

            # TODO check result
            final_results.extend(results)

        if self._skip:
            if len(final_results) <= self._skip:
                return []

            final_results = final_results[self._skip:]

        if self._limit:
            if len(final_results) <= self._skip:
                return final_results

            return final_results[:self._limit]

        return final_results
