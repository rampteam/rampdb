from .base_cmd import BaseCmd
from ..errors import CoordinatorError


class InsertCmd(BaseCmd):

    def __init__(self, ns, options):
        super(InsertCmd, self).__init__(ns, options)
        self._docs = options.get("docs", None)

    def get_targeted_shard(self, doc):
        shard_key_obj = self.get_shard_key_pattern()
        shard_key = shard_key_obj.shard_key_from_doc(doc)
        tk_m = self.get_token_metadata()
        shard = tk_m.get_shard_for_key(shard_key)
        if not shard:
            raise CoordinatorError("Could not find insert shard!")
        return shard

    def run(self):

        results = []
        if not isinstance(self._docs, list):
            self._docs = [self._docs]
        for doc in self._docs:
            result = self.insert_doc(doc)
            results.append(result)

        return self.aggregate(results)

    def insert_doc(self, doc):
        shard = self.get_targeted_shard(doc)
        coll = self.get_db_entity_for_shard(shard)
        result = coll.insert(
            docs=[doc]
        )

        return result

    def aggregate(self, results):
        data = {
            "ids": [],
        }

        for result in results:
            ids = result.get('ids', [])
            data["ids"].extend(ids)

        return data
