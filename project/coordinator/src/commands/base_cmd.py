from ..factory import CoordinatorFactory
from ..shard_key_pattern import ShardKeyPattern
from ..dbclient import Connection


class BaseCmd(object):

    def __init__(self, ns, options={}):
        self._ns = ns
        self._options = options

    def get_shard_key_pattern_for(self, ns):
        collection_metadata = CoordinatorFactory.get_collection_metadata()
        k = collection_metadata.get_key_for_ns_with_retry(ns)
        return ShardKeyPattern(k)

    def get_shard_key_pattern(self):
        return self.get_shard_key_pattern_for(self._ns)

    def get_shards_info(self):
        return CoordinatorFactory.get_shard_info()

    def get_token_metadata(self, ns=None):
        if ns is None:
            ns = self._ns
        return CoordinatorFactory.get_token_metadata(ns)

    def get_all_shards(self):
        return self.get_shards_info().get_all_shards()

    def _get_query_based_targeted_shards(self, query):
        sh_key = self.get_shard_key_pattern()
        token_metadata = self.get_token_metadata()
        hash_value = sh_key.shard_key_from_query(query)
        if not hash_value:
            return self.get_all_shards()
        shard = token_metadata.get_shard_for_key(hash_value)
        if shard:
            return [shard]

        return self.get_all_shards()

    def get_db_entity_for_shard(self, shard):
        conn_string = shard.conn_string
        conn = Connection.from_conn_string(conn_string)
        return conn.get_entity_from_ns(self._ns)
