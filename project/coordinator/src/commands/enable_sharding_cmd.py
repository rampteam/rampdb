from .cluster_base_cmd import ClusterBaseCmd
from ..errors import InvalidOptions


class EnableSharding(ClusterBaseCmd):

    def __init__(self, ns, options):
        super(EnableSharding, self).__init__(ns, options)

    def enable_sharding(self):
        database = self._options.get("database", None)
        if database is None:
            InvalidOptions("Enable sharding expects a database name!")

        return self.get_catalog().enable_sharding(database)

    def shard_collection(self):
        full_ns = self._options.get("name", None)
        options = self._options.get("options", {})
        if full_ns is None:
            InvalidOptions("Shard collection expects a collection name!")

        return self.get_catalog().shard_collection(full_ns, options)
