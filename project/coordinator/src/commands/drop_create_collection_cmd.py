from .cluster_base_cmd import ClusterBaseCmd
from ..errors import CoordinatorError
from ..base.status import Status
from ..model import Token, Collection


class DropCreateCollectionCmd(ClusterBaseCmd):

    def __init__(self, ns, options):
        super(DropCreateCollectionCmd, self).__init__(ns, options)

    def create_collection(self):
        name = self.get('name')
        options = self.get('options')
        shards = self.get_all_shards()
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)
            coll.create_collection(name, options)

        return Status.OK()

    def drop_collection(self):
        name = self.get('name')

        # update metadata
        # remove tokens
        q = {
            "ns": {
                "$regex": self._ns + "." + name
            }
        }

        catalog_query = self.get_catalog_query()

        response = catalog_query.remove(
            Token.get_full_ns(),
            q,
        )

        if not response.status.is_ok():
            ex = "Unable to drop collection to {}! Token metadata remove failed!"
            ex = ex.format(name)
            raise CoordinatorError(ex)

        # remove collection
        q = {
            "full_name": self._ns
        }

        response = catalog_query.remove(
            Collection.get_full_ns(),
            q,
        )

        if not response.status.is_ok():
            ex = "Unable to remove collection! Collection metadata update failed!"
            ex = ex.format(name)
            raise CoordinatorError(ex)

        shards = self.get_all_shards()
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)
            coll.drop_collection(name)
            # TODO check response

        return self.get_catalog_recorder().drop_collection(self._options)
