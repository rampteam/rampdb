from .cluster_base_cmd import ClusterBaseCmd
from ..errors import InvalidOptions
from ..dht import RingManager


class ShardCmd(ClusterBaseCmd):

    def __init__(self, ns, options):
        super(ShardCmd, self).__init__(ns, options)

    def add_shard(self):
        name = self.get("name")
        host = self.get("host")
        max_size = self.get("max_size")

        return RingManager.instance().add_shard(name, host, max_size)

    def remove_shard(self):
        name = self.get("name")
        return RingManager.instance().remove_shard(name)
