from collections import OrderedDict
from .cluster_base_cmd import ClusterBaseCmd
from ..base.status import Status


class IndexOpCmd(ClusterBaseCmd):

    def __init__(self, ns, options):
        super(IndexOpCmd, self).__init__(ns, options)

    def create_index(self):
        name = self.get("name")

        # TODO fix this
        keys = self.get("keys")
        k_list = []
        for k, v in keys.items():
            k_list.append((k, v))

        ord_keys = OrderedDict(k_list)

        options = self.get("options")

        shards = self.get_all_shards()
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)
            coll.create_index(name, ord_keys, **options)

        return Status.OK()

    def drop_index(self):
        ident = self.get("identifier")

        shards = self.get_all_shards()
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)
            coll.drop_index(ident)

        return Status.OK()
