from ..catalog import CatalogManagerFactory
from .base_cmd import BaseCmd


class ClusterBaseCmd(BaseCmd):

    def __init__(self, ns='config', options={}):
        super().__init__(ns, options)

    def get_catalog(self):
        return CatalogManagerFactory.inst().get_catalog()

    def get_catalog_data(self):
        return CatalogManagerFactory.inst().get_catalog_data()

    def get_catalog_query(self):
        query = CatalogManagerFactory.inst().get_catalog_query()
        query.enable_check_results()
        return query

    def get_catalog_recorder(self):
        return CatalogManagerFactory.inst().get_catalog_recorder()

    def get(self, key, default=None):
        return self._options.get(key, default)
