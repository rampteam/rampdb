from .cluster_base_cmd import ClusterBaseCmd
from ..model import Collection


class CollectionNamesCmd(ClusterBaseCmd):

    def __init__(self, ns, options):
        super(CollectionNamesCmd, self).__init__(ns, options)

    def run(self):
        names = []
        # query collection
        q = {
            "full_name": {
                "$regex": self._ns + ".*"
            }
        }
        catalog_query = self.get_catalog_query()

        collections = catalog_query.by_example(
            Collection.get_full_ns(),
            q,
        )

        for collection in collections:
            db, coll = collection["full_name"].split(".")
            names.append(coll)

        return names
