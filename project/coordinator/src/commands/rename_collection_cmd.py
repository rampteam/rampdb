from .cluster_base_cmd import ClusterBaseCmd
from ..errors import CoordinatorError
from ..base.namespace import get_db_from_ns
from ..model import Token, Collection


class RenameCollectionCmd(ClusterBaseCmd):
    def __init__(self, ns, options):
        super(RenameCollectionCmd, self).__init__(ns, options)

    def run(self):
        new = self.get('new')
        shards = self.get_all_shards()
        for sh in shards:
            coll = self.get_db_entity_for_shard(sh)
            coll.rename_collection(new)
            # TODO check response

        # update metadata
        # update tokens
        db = get_db_from_ns(self._ns)
        new_ns = "{}.{}".format(db, new)

        # TODO vendor specific
        q = {
            "ns": self._ns
        }

        d = {
            "$set": {
                "ns": new_ns
            }
        }
        catalog_query = self.get_catalog_query()

        response = catalog_query.update(
            Token.get_full_ns(),
            q,
            d
        )

        if not response.status.is_ok():
            ex = "Unable to rename collection to {}! Token metadata update failed!"
            ex = ex.format(new)
            raise CoordinatorError(ex)

        # update collection
        q = {
            "full_name": self._ns
        }

        d = {
            "$set": {
                "full_name": new_ns
            }
        }
        catalog_query = self.get_catalog_query()

        response = catalog_query.update(
            Collection.get_full_ns(),
            q,
            d
        )

        if not response.status.is_ok():
            ex = "Unable to rename collection to {}! Collection metadata update failed!"
            ex = ex.format(new)
            raise CoordinatorError(ex)

        return self.get_catalog_recorder().rename_collection(self._options)
