from .cluster_base_cmd import ClusterBaseCmd


class DatabaseCustomCmd(ClusterBaseCmd):
    def __init__(self, ns, options):
        super(DatabaseCustomCmd, self).__init__(ns, options)

    def run(self):
        name = self.get("name")
        options = self.get("options")

        shards = self.get_all_shards()
        results = []
        for sh in shards:
            db = self.get_db_entity_for_shard(sh)
            res = db.run_command(name, options)
            results.append(res)


        return self._merge_results(results)


    def _merge_results(self, results):
        if results:
            return results[0]

        return results
