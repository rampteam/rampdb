import logging
from threading import Thread

from src.config import settings


class StopThread(Thread):
    _should_run = False
    __logger = None

    def __init__(self, *args, **kwargs):
        self._should_run = True
        super().__init__(*args, **kwargs)

    def terminate(self):
        self._should_run = False

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger
