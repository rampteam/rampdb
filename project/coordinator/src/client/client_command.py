class ClientCommand(object):

    def __init__(self, name, ns, params, cmd_id=None):
        self._name = name
        self._ns = ns
        self._params = params
        self.__id = cmd_id

    @property
    def ns(self):
        return self._ns

    @property
    def name(self):
        return self._name

    @property
    def params(self):
        return self._params

    @property
    def cmd_id(self):
        return self.__id

    def __eq__(self, other):
        if not other:
            return False

        if not isinstance(other, ClientCommand):
            return False

        if self.cmd_id is not None:
            if self.cmd_id != other.cmd_id:
                return False

        n_eq = self.name == other.name
        ns_eq = self.ns == other.ns
        p_eq = self.params == other.params
        id_eq = self.cmd_id == other.cmd_id

        return n_eq and ns_eq and p_eq and id_eq

    def __str__(self):
        msg = "ClientCommand("
        msg += "name {}"
        msg += "ns {}"
        msg += "params {}"
        msg += "id {}"

        return msg.format(
            self.name,
            self.ns,
            self.params,
            self.cmd_id
        )







