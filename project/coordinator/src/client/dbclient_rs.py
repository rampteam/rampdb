class ReadPreference(object):

    def __init__(self, type):
        self.__type = type

    def is_primary(self):
        return self.__type == "primary"

    def is_primary_preferred(self):
        return self.__type == "primary_preferred"

    def is_secondary(self):
        return self.__type == "secondary"

    def is_secondary_peferred(self):
        return self.__type == "secondary"

    def is_nearest(self):
        return self.__type == "nearest"

    @classmethod
    def primary(self):
        return ReadPreference("primary")

    @classmethod
    def primary_preferred(self):
        return ReadPreference("primary_preferred")

    @classmethod
    def secondary(self):
        return ReadPreference("secondary")

    @classmethod
    def secondary_peferred(self):
        return ReadPreference("secondary")

    @classmethod
    def nearest(self):
        return ReadPreference("nearest")

    def __str__(self):
        msg = "ReadPreference ( {} )"
        return msg.format(str(self.__type))
