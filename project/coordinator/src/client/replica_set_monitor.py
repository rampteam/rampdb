from .dbclient_rs import ReadPreference
from ..dbclient import HostAndPort


class ReplicaSetMonitor(object):

    def __init__(self, name, member_hosts):
        super(ReplicaSetMonitor, self).__init__()
        self._name = name
        self._member_hosts = member_hosts

    @property
    def name(self):
        return self._name

    @property
    def members_hosts(self):
        return self._member_hosts

    def get_host_or_refresh(self, criteria: ReadPreference):
        """ Return a member host matching criteria
            empty HostAndPort otherwise
        """
        pass

    def get_master(self):
        """
        Return current master
        """
        pass

    def failed_host(self, host: HostAndPort):
        """
        Notify this Monitor that a host has failed
        and should be considered down.
        """
        pass

    def is_primary(self, host: HostAndPort):
        """
        Return true if this node is master taking local data only into account.
        """
        pass

    def is_host_up(self, host: HostAndPort):
        """
        Return true host is member of this replica set and is up
        """
        pass

    def contains(self, host: HostAndPort):
        """
        Return true host is member of this replica set
        """
        pass

    def get_connection_string(self):
        pass
