from ..dbclient import Connection
from ..dbclient import ConnectionString
from .client_command import ClientCommand


class CommandDispatch(object):

    def __init__(self):
        super(CommandDispatch, self).__init__()
        self._pending = []

    def add_command(self, conn: ConnectionString, cmd: ClientCommand):
        self._pending.append((conn, cmd))

    def num_pending(self):
        return len(self._pending)

    def send_all(self):
        data = []
        for conn, cmd in self._pending:
            result = self.execute_command(conn, cmd)
            data.append(conn, cmd, result)

        return data

    def execute_command(self, conn_string, cmd):
        # TODO check if this is the right place for this command
        conn = Connection.from_conn_string(conn_string)
        return conn.exec_command(
            cmd.name,
            cmd.ns,
            cmd.params,
        )

    def reset(self):
        self._pending = []
