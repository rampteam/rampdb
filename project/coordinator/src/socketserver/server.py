import concurrent.futures as cf
import logging
import socket

from src.config import settings


class Server():
    connection = None
    __should_run = True
    __connection_attempts = 5
    __reuse_addr = 1
    start_success = None

    def __init__(self,
                 connection: tuple,
                 start_success,
                 request_handler
                 ):
        """

        :param connection:
        :param start_success: set to 1 if server is started with success, 0 if not
        :param request_handler:
        :return:
        """
        self.start_success = False
        self.request_handler = request_handler
        self.connection = connection
        self.logger = logging.getLogger(settings.get('logger_name'))
        self._workers = settings.get(
            'client_number_of_threads', default=5, section='server')
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_bind()

    def server_bind(self):
        try:
            self.server.setsockopt(
                socket.SOL_SOCKET, socket.SO_REUSEADDR, self.__reuse_addr)
            self.server.bind(self.connection)

            # maximum queried connection attempts
            self.server.listen(self.__connection_attempts)
            self.start_success = 1
            addr = self.server.getsockname()
        except Exception as e:
            self.logger.critical(str(e))
            self.start_success = 0

    def serve_forever(self):
        with cf.ThreadPoolExecutor(max_workers=self._workers) as executor:

            while self.__should_run:
                try:
                    self.logger.info('Accepting')
                    clientsock, addr = self.server.accept()
                    self.logger.info('Accepted from {}'.format(addr))
                    # CatalogManagerFactory.set_instance(self._factory)
                    handler = self.request_handler(clientsock, addr)
                    executor.submit(handler.handle)
                except IOError as e:
                    if e.errno != socket.errno.EINTR:
                        raise

    def shutdown(self):
        self.__should_run = False
        self.server.close()
