import logging
import traceback

from ._communication import receive, send
from ..errors import SocketRecvError, SocketTimeoutError, SocketError
from ..config import settings
from ..pool import ObjectPool, acquire_object
from ..wire import CoordinatorProtocol

pool = ObjectPool(CoordinatorProtocol, max_size=5)


class RequestHandler():
    logger = None

    def __init__(self, request, client_addr):
        self.request = request
        self.client_addr = client_addr
        self.logger = logging.getLogger(settings.get('logger_name'))

    def handle(self):
        self.request.settimeout(float(settings.get('socket_timeout', default=300.0, section='server')))
        try:
            self.logger.debug('Getting message')
            message = receive(self.request)
            with acquire_object(pool) as coord_protocol_handler:
                response = coord_protocol_handler.data_received(message)
                self.logger.info(response)
            send(self.request, response)
        except SocketRecvError as e:
            self.logger.warning(str(e))
        except (SocketTimeoutError, SocketError) as e:
            self.logger.info(str(e))
            self.request.close()
        except Exception as e:
            self.logger.critical(str(e))
            self.logger.critical(traceback.format_exc())
        finally:
            self.request.close()
