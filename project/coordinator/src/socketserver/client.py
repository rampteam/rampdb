from socket import (
    socket,
    AF_INET,
    SOCK_STREAM,
)
from ._communication import send, receive


class Client:
    timeout = 30

    def __init__(self, host, port, timeout):
        self.conn = (host, port)
        self.timeout = timeout

    def send(self, message:bytes):
        sock = None
        try:
            sock = socket(AF_INET, SOCK_STREAM)
            sock.settimeout(self.timeout)
            sock.connect(self.conn)
            send(sock, message)
            return receive(sock)
        except Exception:
            raise
        finally:
            if sock:
                sock.close()
