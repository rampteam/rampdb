from ..errors import InvalidOptions, CoordinatorError
from ..model import Shard, Token, Database, Collection
from ..dates import current_utc_date
from ..base.namespace import (
    ns_is_db_only,
    get_db_from_ns
)
from ..base import Status


class CatalogManager(object):

    def __init__(self, query, data, recorder):
        self._query = query
        self._data = data
        self._changelog = recorder

    def enable_sharding(self, database):
        try:
            if not ns_is_db_only(database):
                raise InvalidOptions("bad database name: {}".format(database))

            db_model = self.get_database(database)

            if not db_model:
                # shard_info = ShardInfo.instance()
                # primary = shard_info.pick()
                # if not primary:
                # return Status(
                #         ErrorCodes.ShardNotFound,
                #         "can't find a shard to put new db on"
                #     )

                db_model = Database(database, None, True)
            else:
                db_model.sharded = True

            status_update = self.update_database(database, db_model)

            if not status_update.is_ok():
                return status_update

            return self._changelog.enable_sharding(database)

        except Exception as e:
            raise
            return Status.from_ex(e)

    def shard_collection(self, ns, options):

        # make sure sharding is enabled
        db = get_db_from_ns(ns)

        self.enable_sharding(db)

        d = {
            "full_name": ns,
            "updated_at": current_utc_date()
        }

        if "key" not in options:
            raise InvalidOptions("no shard key specified!")

        d.update(options)

        coll_model = self.get_collection(ns)

        if coll_model:
            raise CoordinatorError("Collection {} already sharded!".format(ns))

        cmd_response = self._query.insert(
            Collection.get_full_ns(),
            d
        )
        if not cmd_response.status.is_ok():
            return Status(cmd_response.status.code(),
                          "collection metadata sharding write failed: " +
                          str(cmd_response.status)
                          )

        return self._changelog.shard_collection(ns, options)

    def update_database(self, db_name, db_model):

        cmd_response = self._query.update(
            Database.get_full_ns(),
            {"name": db_name},
            db_model.to_dict(),
            True,
            False
        )

        if not cmd_response.status.is_ok():
            return Status(cmd_response.status.code(),
                          "database metadata sharding write failed: " +
                          str(cmd_response.status)
                          )

        return self._changelog.update_database(db_name, db_model)

    def update_collection(self, coll_name, coll_model):

        cmd_response = self._query.update(
            Collection.get_full_ns(),
            {"name": coll_name},
            coll_model.to_dict(),
            True,
            False
        )

        if not cmd_response.status.is_ok():
            return Status(cmd_response.status.code(),
                          "collection metadata sharding write failed: " +
                          str(cmd_response.status)
                          )

        return self._changelog.update_database(coll_name, coll_model)

    def is_valid_shard(self, name, conn_string):
        # TODO implement later
        pass

    def add_shard(self, name, host, max_size):
        # host: string
        # TODO add more specific validation
        # build the ConfigDB shard document
        # finally

        shard_model = Shard(
            name,
            host,
            max_size
        )

        response = self._query.insert(
            Shard.get_full_ns(),
            shard_model.to_dict()
        )

        if not response.status.is_ok():
            return response.status

        return self._changelog.add_shard(
            name, host, max_size
        )

    def remove_shard(self, name):

        response = self._query.remove(
            Shard.get_full_ns(),
            {"name": name}
        )

        if not response.status.is_ok():
            return response.status

        return self._changelog.remove_shard(name)

    def init(self, config_conn):
        self._cfg_conn = config_conn

    def update_token_ops(self, ops):
        if isinstance(ops, list):
            return self._query.update_token_op(ops)

    def update_token_op(self, data):
        # TODO this should be in some kind of transaction
        target_node = data['target']
        shard_name = target_node
        op = data['op']
        op_data = data['data']

        status = False
        if op == "set":
            status = self.set_tokens(op_data, shard_name)

        if op == "move_to_set":
            status = self.move_to_shard_tokens(op_data, shard_name)

        if op == "move_to_remove":
            status = self.move_to_shards_tokens(op_data, shard_name)

        if status is False:
            raise ValueError("Unknown token op : {}!".format(op))

        if not status.is_ok():
            return status

        return self._changelog.update_token_op(data)

    def check_token_op(self, response):
        fail_msg = "unable to write token metadata ops Err: {}"
        if not response.status.is_ok():
            return Status(response.status.code(),
                          fail_msg.format(str(response.status))
                          )
        return Status.OK()

    def set_tokens(self, op_data, shard_name):
        for token in op_data:
            token.shard = shard_name
            d = token.to_dict()
            d["updated_at"] = current_utc_date()
            response = self._query.insert(
                Token.get_full_ns(),
                d
            )

            last_status = self.check_token_op(response)
            if not last_status.is_ok():
                return last_status

        return Status.OK()

    def move_to_shard_tokens(self, op_data, target_shard_name):
        for sh, tokens in op_data.items():
            for token in tokens:
                token.shard = target_shard_name
                q = {
                    "name": token.name
                }

                d = token.to_dict()
                d["updated_at"] = current_utc_date()
                d['shard'] = target_shard_name
                response = self._query.update(
                    Token.get_full_ns(),
                    q,
                    d
                )
                last_status = self.check_token_op(response)
                if not last_status.is_ok():
                    return last_status

        return Status.OK()

    def move_to_shards_tokens(self, op_data, source_shard_name):
        for sh, tokens in op_data.items():
            for token in tokens:
                token.shard = sh
                q = {
                    "name": token.name
                }

                d = token.to_dict()
                d["updated_at"] = current_utc_date()
                d['shard'] = sh
                response = self._query.update(
                    Token.get_full_ns(),
                    q,
                    d
                )
                last_status = self.check_token_op(response)
                if not last_status.is_ok():
                    return last_status

        return Status.OK()

    def get_database(self, db_name):
        """
        Retrieves the metadata for a given database.
        """

        db_obj = self._query.one_example(
            Database.get_full_ns(),
            {"name": db_name}
        )

        if db_obj:
            return Database.from_dict(db_obj)

    def get_collection(self, coll_name):
        """
        Retrieves the metadata for a given database.
        """

        db_obj = self._query.one_example(
            Collection.get_full_ns(),
            {"full_name": coll_name}
        )

        if db_obj:
            return Collection.from_dict(db_obj)
