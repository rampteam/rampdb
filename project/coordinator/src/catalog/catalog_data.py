from multiprocessing import Manager
from copy import deepcopy

from ..model import Shard, Token, Database, Collection, ChangeLog
from ..dates import current_utc_date


class CatalogData(object):
    _manager = None
    _should_pool = True

    def __init__(self, catalog_query):
        self._manager = Manager()
        self.tokens = self._manager.dict()
        self.shards = self._manager.list()
        self.collections = self._manager.list()
        self._query = catalog_query
        self._reload()
        self._last_update = current_utc_date()

    def get_tokens(self, ns):
        if not self.tokens:
            self._reload_tokens()

        return self.tokens.get(ns, [])

    def _reload_tokens(self):
        self.tokens = self._manager.dict()
        tk_data = {}
        tokens_coll = self._query.by_example(
            Token.get_full_ns(),
            {}
        )

        for token in tokens_coll:
            if token["ns"] not in tk_data:
                tk_data[token["ns"]] = []

            tk_data[token["ns"]].append(token)
        self.tokens = tk_data

        return tokens_coll

    def get_token_models(self, ns):
        token_list = []
        tokens_coll = self.get_tokens(ns)
        for token in tokens_coll:
            token_list.append(Token.from_dict(token))

        return token_list

    def get_all_shards(self):
        if self.shards:
            return deepcopy(self.shards)
        else:
            return self._reload_all_shards()

    def _reload_all_shards(self):
        self.shards = self._manager.list()
        shards_coll = self._query.by_example(
            Shard.get_full_ns(),
            {}
        )

        for shard_d in shards_coll:
            self.shards.append(shard_d)

        return shards_coll

    def get_all_shard_models(self):
        shard_list = []
        shards_coll = self.get_all_shards()
        for shard_d in shards_coll:
            shard_list.append(Shard.from_dict(shard_d))

        return shard_list

    def get_all_collections(self):
        if self.collections:
            return deepcopy(self.collections)
        else:
            return self._reload_all_collections()

    def _reload_all_collections(self):
        self.collections = self._manager.list()
        collections = self._query.by_example(
            Collection.get_full_ns(),
            {}
        )

        for coll in collections:
            self.collections.append(coll)

        return collections

    def are_changes(self):
        # TODO REFACTOR QUERY
        result = self._query.one_example(
            ChangeLog.get_full_ns(),
            {
                "time": {"$gt": self._last_update}
            }
        )

        # TODO check doc count here
        if result:
            return True
        else:
            return False

    def reload_if_changes(self):
        if self._should_pool and self.are_changes():
            self._reload()

    def _reload(self):
        if not self._should_pool:
            return
        self._reload_all_shards()
        self._reload_tokens()
        self._reload_all_collections()
        self._last_update = current_utc_date()

    def stop_pooling(self):
        self._should_pool = False
        self._manager.shutdown()
        self._manager.join()
