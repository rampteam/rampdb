from .catalog_manager import CatalogManager
from .catalog_query import CatalogQuery
from .catalog_data import CatalogData
from .changelog_recorder import ChangeLogRecorder
from .validator import validate_cfg_hosts


class CatalogManagerFactory(object):

    __instance = None

    def __init__(self, config_servers):
        self.__manager = None
        self.__query = None
        self.__data = None
        self.__recorder = None
        self.__config_conn = validate_cfg_hosts(config_servers)

    def get_cfg_conn(self):
        if not self.__config_conn:
            raise Exception("Config connections not initialized!")
        return self.__config_conn

    @classmethod
    def set_instance(cls, instance):
        cls.__instance = instance

    @classmethod
    def inst(cls, config_servers=None):
        if config_servers is None:
            if cls.__instance:
                return cls.__instance
            else:
                raise RuntimeError("Config servers not initialized!")

        cls.__instance = CatalogManagerFactory(config_servers)
        return cls.__instance

    def get_catalog(self):
        if self.__manager is not None:
            return self.__manager

        if self.__config_conn is not None:

            query = self.get_catalog_query()
            data = self.get_catalog_data()
            recorder = self.get_catalog_recorder()

            self.__manager = CatalogManager(query, data, recorder)
            return self.__manager

        raise Exception("Catalog not initialized!")

    def get_catalog_query(self):
        if self.__query is not None:
            return self.__query

        self.__query = CatalogQuery(self.get_cfg_conn())
        return self.__query

    def get_catalog_data(self):
        if self.__data is not None:
            return self.__data

        catalog_quey = self.get_catalog_query()
        self.__data = CatalogData(catalog_quey)
        return self.__data

    def get_catalog_recorder(self):
        if self.__recorder is not None:
            return self.__recorder

        catalog_quey = self.get_catalog_query()
        self.__recorder = ChangeLogRecorder(catalog_quey)
        return self.__recorder

    def set_catalog_data(self, obj):
        if isinstance(obj, CatalogData):
            self.__data = obj

        raise ValueError("Invalid catalog data initializer!")
