from ..client import CommandDispatch
from ..client import ClientCommand
from .config_coordinator import ConfigCoordinator
from ..base import Status, ErrorCodes


class CatalogQuery(object):
    def __init__(self, cfg_conn):
        self._dispatcher = CommandDispatch()
        self._cfg_coord = ConfigCoordinator(cfg_conn, self._dispatcher)
        self.disable_check_results()

    def enable_check_results(self):
        self._check_results = True

    def disable_check_results(self):
        self._check_results = False

    def insert(self, ns, doc):
        p = {
            "docs": doc
        }
        cmd = ClientCommand("insert", ns, p)

        return self.write_to_cfg_servers(cmd)

    def update(self, ns, query, update, upsert=False, multi=False):
        p = {
            "spec": query,
            "document": update,
            "upsert": upsert,
            "multi": multi,
        }

        cmd = ClientCommand("update", ns, p)

        return self.write_to_cfg_servers(cmd)

    def remove(self, ns, query, single=False):
        p = {
            "spec": query,
            "single": single,
        }

        cmd = ClientCommand("remove", ns, p)

        return self.write_to_cfg_servers(cmd)

    def by_example(self, ns, spec=None, skip=0, limit=0, fields=None, cursor=False, **kwargs):
        p = {
            "spec": spec,
            "skip": skip,
            "limit": limit,
            "fields": fields,
            "cursor": cursor,
        }

        p.update(kwargs)

        cmd = ClientCommand("by_example", ns, p)

        return self.query_cfg_servers(cmd)

    def one_example(self, ns, spec=None, fields=None, **kwargs):
        res = self.by_example(
            ns,
            spec,
            0,
            1,  # limit
            fields,
            **kwargs
        )

        if not res:
            return {}

        if isinstance(res, list):
            if len(res) > 0:
                return res[0]

    def return_write_status(self, cmd_response, msg_err):
        if not cmd_response.status.is_ok():
            return Status(cmd_response.status.code(),
                msg_err + str(cmd_response.status)
            )

    def write_to_cfg_servers(self, cmd):
        cmd_response = self._cfg_coord.execute_cmd(cmd)
        if self._check_results and not cmd_response.status.is_ok():
            raise cmd_response.status.to_exception()
        return cmd_response

    def query_cfg_servers(self, cmd):
        # TODO Status validation
        cmd_response = self._cfg_coord.query_cmd(cmd)
        if not cmd_response.status.is_ok():
            raise cmd_response.status.to_exception()

        return cmd_response.any()


        