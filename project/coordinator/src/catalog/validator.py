from ..errors import InvalidOptions, CoordinatorError
from ..dbclient import ConnectionString


def validate_cfg_hosts(config_hosts):

    if len(config_hosts) != len(set(config_hosts)):
        raise InvalidOptions("Duplicate Config servers hosts!")

    cfg_conn_strings = ConnectionString.from_conn_strings(config_hosts)
        # TODO check servers is up! AKA resolve DNS

    return cfg_conn_strings
