from ..config import settings
from ..model import ChangeLog
from ..base import Status


class ChangeLogRecorder(object):

    def __init__(self, catalog_query):
        self._query = catalog_query

    def enable_sharding(self, database):
        data = {
            "what": "enable_sharding",
            "ns": database,
            "details": {
                "database": database
            }
        }

        return self.record_changelog(data)

    def shard_collection(self, ns, options):
        options["ns"] = ns
        data = {
            "what": "shard_collection",
            "ns": ns,
            "details": options
        }

        return self.record_changelog(data)

    def update_database(self, database, db_model=None):
        data = {
            "what": "update_database",
            "ns": database,
            "details": {
                "database": database
            }
        }

        return self.record_changelog(data)

    def update_colelction(self, collection, coll_model=None):
        data = {
            "what": "update_collection",
            "ns": collection,
            "details": {
                "collection": collection
            }
        }

        return self.record_changelog(data)

    def add_shard(self, name, host, max_size):
        data = {
            "what": "add_shard",
            "details": {
                "name": name,
                "host": host,
                "max_size": max_size,
            }
        }

        return self.record_changelog(data)

    def remove_shard(self, name):
        data = {
            "what": "remove_shard",
            "details": {
                "name": name,
            }
        }

        return self.record_changelog(data)

    def update_token_op(self, op_data):
        data = {
            "what": "token_op",
            "details": {
                "target_node": op_data['target'],
                "op": op_data['op'],
            }
        }

        return self.record_changelog(data)

    def rename_collection(self, op_data):
        data = {
            "what": "rename_collection",
            "details": op_data
        }

        return self.record_changelog(data)

    def drop_collection(self, op_data):
        data = {
            "what": "drop_collection",
            "details": op_data
        }

        return self.record_changelog(data)

    def record_changelog(self, data):

        if "server" not in data:
            data["server"] = self.get_current_host()

        ch_model = ChangeLog.from_dict(data)

        response = self._query.insert(
            ChangeLog.get_full_ns(),
            ch_model.to_dict()
        )

        if not response.status.is_ok():
            return response.status

        return Status.OK()

    def get_current_host(self):
        host = settings.get('client_host', '127.0.0.1', section='server')
        port = settings.get('client_port', section='server')
        return "{}:{}".format(
            host, port
        )
