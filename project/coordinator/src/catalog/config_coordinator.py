from ..base import Status
from ..errors import ConfigServerMismatchError


class ConfigCoordinator(object):

    def __init__(self, cfg_conns, dispatcher):
        self._cfg_conn = cfg_conns
        self._dispatcher = dispatcher

    def execute_cmd(self, cmd):
        cmd_responses = ConfigCmdResponse(cmd)
        for cfg_conn_str in self._cfg_conn:
            result = self._dispatcher.execute_command(cfg_conn_str, cmd)
            cmd_responses.add(cfg_conn_str, result)

        cmd_responses.are_ok()
        return cmd_responses

    def query_cmd(self, cmd):
        cmd_responses = self.execute_cmd(cmd)
        cmd_responses.validate_identical_responses()
        return cmd_responses


class ConfigCmdResponse(object):

    def __init__(self, cmd):
        self._cmd = cmd
        self._responses = {}
        self._status = Status()

    def any(self):
        return self.first()

    def first(self):
        return next(iter(self._responses.values()))

    def add(self, conn_obj, response):
        self._responses[conn_obj] = response

    def delete(self, conn_obj):
        return self._responses.pop(conn_obj, None)

    def are_ok(self):
        try:
            for conn, response in self._responses.items():
                self.validate_result(response)
        except Exception as e:
            self._status = Status.from_ex(e)

    def are_identical(self):
        r = list(self._responses.values())
        ln = len(r)
        if ln == 1:
            return True
        prev = r[0]
        i = 1
        while i < ln:
            current = r[i]
            if current != prev:
                return False

        return True

    def validate_identical_responses(self):
        if not self.are_identical():
            ex = ConfigServerMismatchError(
                "Different responses from cfg servers!")
            self._status = Status.from_ex(ex)

    def validate_result(self, result):
        msg = "Could not execute cmd on config servers! Response {} "
        ex = Exception(msg.format(result))

        # if not result:
        #     raise ex

        # case of query
        if not isinstance(result, dict):
            return

        ok = result.get("ok", 0)
        if not ok:
            raise ex

    @property
    def cmd(self):
        return self._cmd

    @property
    def status(self):
        return self._status
