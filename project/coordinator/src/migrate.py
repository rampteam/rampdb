import datetime
from .distributed_lock import DistributedLock
from .dbclient import ConnectionString, Connection
from .shard_key_pattern import ShardKeyPattern
from .base import Status, ErrorCodes
from .collection_metadata import CollectionMetadata
from .dht import PartitionerFactory


class RecvMoveToken(object):

    def __init__(self, options):
        self._full_ns = options.get("full_ns", None)
        self._from_shard = options.get("from_shard", None)
        self._to_shard = options.get("to_shard", None)
        self._query = options.get("query", None)
        self._sync_from = options.get("sync_from", None)

    def run(self):

        # Find and insert docs from target
        from_coll = self.get_from_collection()
        docs = from_coll.by_example(self._query)

        to_coll = self.get_to_collection()
        response = to_coll.insert(docs)

        check = ["writeError", "writeConcernError"]
        for ch in check:
            data = response.get(ch, None)
            if data is not None:
                raise Exception(
                    "Insert move token failed! Error: {}".format(data))

        # print(response)
        return Status.OK()

    def get_from_collection(self):
        conn_string = ConnectionString.from_text(self._from_shard)
        conn = Connection.from_conn_string(conn_string)
        return conn.get_entity_from_ns(self._full_ns)

    def get_to_collection(self):
        conn_string = ConnectionString.from_text(self._to_shard)
        conn = Connection.from_conn_string(conn_string)
        return conn.get_entity_from_ns(self._full_ns)


class MoveTokenCommand(object):

    def __init__(self, options):
        self._full_ns = options.get("full_ns", None)
        self._from_shard = options.get("from_shard", None)
        self._to_shard = options.get("to_shard", None)
        self._min = options.get("min", None)
        self._max = options.get("max", None)
        self._wait_for_delete = options.get("wait_for_delete", None)

    def run(self):
        """
        Lock the distributed lock to ensure shard metadata stability
        Migration retrieve all Documents Ids that neeeds to be migrated
        """
        lock = DistributedLock()
        lock.busy_acquire(
            self._full_ns,
            "token_balancer",
            "balance ring token"
        )

        sync_from = datetime.datetime.now()

        record_ids = self.get_token_docs()

        lock.release()

        query = {
            "_id": {
                "$in": record_ids
            }
        }

        p = {
            "name": "recv_move_token",
            "full_ns": self._full_ns,
            "from_shard": self._from_shard,
            "to_shard": self._to_shard,
            "query": query,
            "sync_from": sync_from,
        }

        # Run locally for now move sister command on client
        # to_conn = self.get_to_connection()
        # try:
        # response = to_conn.exec_command("run_command", "admin", p)
        # except Exception as e:
        #     return Status.from_ex(e)

        recv_cmd = RecvMoveToken(p)
        recv_cmd.run()

        # DELETE PHASE
        # TODO when adding cursors add support for staling delete until all cursors expired
        # print(query)

        # cursors
        from_coll = self.get_from_collection()
        data = from_coll.remove(query)

        # print(data)

        nr = data.get('n', 0)
        removed = len(record_ids)
        if nr != len(record_ids):
            msg = "Not all ids were removed! Expected {} removed {} response {}"
            msg = msg.format(nr, removed, data)
            return Status(ErrorCodes.OperationIncomplete, msg)

        return Status.OK()

    def get_to_connection(self):
        conn_string = ConnectionString.from_text(self._to_shard)
        return Connection.from_conn_string(conn_string)

    def get_from_collection(self):
        conn_string = ConnectionString.from_text(self._from_shard)
        conn = Connection.from_conn_string(conn_string)
        return conn.get_entity_from_ns(self._full_ns)

    def get_token_docs(self):

        key_factory = PartitionerFactory.get_key_factory()

        min_key = key_factory.from_long((self._min))
        max_key = key_factory.from_long((self._max))

        from_coll = self.get_from_collection()
        records = from_coll.by_example()

        col_metadata = CollectionMetadata.instance()
        shard_key_doc = col_metadata.get_key_for_ns(self._full_ns)
        key_pattern = ShardKeyPattern(
            shard_key_doc
        )

        records_ids = []
        for doc in records:
            k = key_pattern.shard_key_from_doc(doc)
            if min_key <= k < max_key:
                # Warning assumption of a _id elem in every doc
                records_ids.append(doc["_id"])

        return records_ids
