from copy import deepcopy

from .catalog.factory import CatalogManagerFactory
from .dbclient import ConnectionString
from .errors import ShardDataNotFound
from random import choice


class TypeOfShard(object):

    def __init__(self, type):
        self.__type = type

    def is_standalone(self):
        return self.__type == "standalone"

    def is_replica(self):
        return self.__type == "replica"

    @classmethod
    def standalone(cls):
        return cls("standalone")

    @classmethod
    def replica(cls):
        return cls("replica")

    def __str__(self):
        return "ShardType({})".format(self.__type)


class ShardFactory(object):

    @classmethod
    def from_model(cls, model):
        conn = ConnectionString.from_text(model.host)
        if conn.type.is_set():
            return ReplicaShard(model)

        return StandaloneShard(model)


class BaseShard(object):

    def __init__(self, model, tp=None):
        self.__type = tp
        self.__model = model
        self.__conn_string = ConnectionString.from_text(self.model.host)

    @property
    def type(self):
        return self.__type

    @property
    def conn_string(self):
        return self.__conn_string

    def is_standalone(self):
        return self.type.is_standalone()

    def is_replica(self):
        return self.type.is_replica()

    @property
    def model(self):
        return self.__model

    @property
    def name(self):
        return self.__model.name

    @property
    def host(self):
        return self.__model.host

    def __str__(self):
        return self.model.__str__()

    def __repr__(self):
        return self.model.__repr__()

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def __hash__(self):
        return hash(self.name) ^ hash(self.type) ^ hash(self.model.host)


class StandaloneShard(BaseShard):

    def __init__(self, model):
        super().__init__(model, TypeOfShard.standalone())


class ReplicaShard(BaseShard):

    def __init__(self, model):
        super().__init__(model, TypeOfShard.standalone())
        self.init()

    def init(self):
        self._members_hosts = deepcopy(self.__conn_string.host_and_ports)
        self._replica_name = self.__conn_string.replica_name

    @property
    def members_hosts(self):
        return self._members_hosts

    @property
    def replica_name(self):
        return self.__replica_name


class ShardInfo(object):

    __instance = None

    def __init__(self):
        # Map of both shardName -> Shard and hostName -> Shard
        self._lookup = {}
        self._rs_lookup = {}
        self.reload()

    @classmethod
    def instance(cls):
        if not cls.__instance:
            cls.__instance = cls()
        return cls.__instance

    def reload(self):
        catalog = CatalogManagerFactory.inst().get_catalog_data()
        shard_list = catalog.get_all_shard_models()

        for shard_model in shard_list:
            sh = ShardFactory.from_model(shard_model)
            self._lookup[sh.model.name] = sh

            if sh.is_replica():
                self._rs_lookup[sh.replica_name] = sh
                for host_port in sh.members_hosts:
                    self._lookup[str(host_port)] = sh
            else:
                self._lookup[sh.model.host] = sh

    def get_all_shards(self):
        added = {}
        shards = []
        for name_host, sh in self._lookup.items():
            if sh.model.name not in added:
                shards.append(sh)
                added[sh.model.name] = 1

        return shards

    def shards_count(self):
        sh = self.get_all_shards()
        return len(sh)

    def remove(self, name):

        new_lookup = {}
        host = None
        for name_host, sh in self._lookup.items():
            if sh.model.name == name:
                host = sh.model.host
                continue

            if sh.model.host == host:
                continue

            new_lookup[name_host] = sh

        self._lookup = new_lookup

        new_rs = {}
        for name_host, sh in self._rs_lookup.items():
            if sh.replica_name == name:
                continue

            new_rs[name_host] = sh

        self._rs_lookup = new_rs

    def find_by_host(self, ident):
        conn = ConnectionString.from_text(ident)
        if conn.type.is_set():
            set_name = conn.replica_name
            if set_name is self._rs_lookup:
                return self._rs_lookup[set_name]
        else:
            if ident in self._lookup:
                return self._lookup[ident]

        return None

    def find_by_host_with_retry(self, ident):

        sh = self.find_by_host(ident)

        if sh is not None:
            return sh

        # we don't have it reload
        self.reload()

        sh = self.find_by_host(ident)

        if sh is None:
            raise ShardDataNotFound("can't find shard for: {}".format(ident))

        return sh

    def find_by_name(self, name):
        if name in self._lookup:
            return self._lookup[name]

        return None

    def find_by_name_with_retry(self, ident):

        sh = self.find_by_name(ident)

        if sh is not None:
            return sh

        # we don't have it reload
        self.reload()

        sh = self.find_by_name(ident)

        if sh is None:
            raise ShardDataNotFound("can't find shard for: {}".format(ident))

        return sh

    def pick(self):
        # TODO add this based on datasize
        shards = self.get_all_shards()
        return choice(shards)

    def get_shard_names(self):
        shards = self.get_all_shards()
        names = []
        for sh in shards:
            names.append(sh.name)
        return names

    def get_names_to_host(self):
        shards = self.get_all_shards()
        data = {}
        for sh in shards:
            data[sh.name] = sh.host

        return data
