from .dht import PartitionerFactory
from .util.types import is_alphanumeric


class ShardKeyPattern(object):

    def __init__(self, key_pattern):
        self._key_pattern = key_pattern
        if not self.is_hashed():
            raise NotImplemented("Order shard keys not implemented!")

    @property
    def key_pattern(self):
        return self._key_pattern

    def is_hashed(self):
        k = self.key_pattern
        for elem, sort in k.items():
            if sort != 'hashed':
                return False
        return True

    def extract_shard_key_from_query(self, query):
        # TODO add support for multiple query operatos
        key_factory = PartitionerFactory.get_key_factory()
        sh_key = {}
        for elem, sort in self._key_pattern.items():
            val = query.get(elem, None)
            # TODO add support for multiple query operators
            if val is None or not is_alphanumeric(val):
                continue
            hashed_token = key_factory.from_string(str(val))
            sh_key[elem] = hashed_token

        return sh_key

    def extract_first_shard_key_from_query(self, query):
        sh_key = self.extract_shard_key_from_query(query)
        val = self.first_value(sh_key)
        return val

    def shard_key_from_query(self, query):
        return self.extract_first_shard_key_from_query(query)

    def extract_shard_key_from_doc(self, doc):
        key_factory = PartitionerFactory.get_key_factory()

        sh_key = {}
        for elem, sort in doc.items():
            str_val = str(doc.get(elem, None))
            hashed_token = key_factory.from_string(str_val)
            sh_key[elem] = hashed_token

        return sh_key

    def extract_first_shard_key_from_doc(self, doc):
        sh_key = self.extract_shard_key_from_doc(doc)
        val = self.first_value(sh_key)
        return val

    def shard_key_from_doc(self, doc):
        return self.extract_first_shard_key_from_doc(doc)

    def first_value(self, m_dict, default=None):
        if not m_dict:
            return default

        return next(iter(m_dict.values()))
        # return list(m_dict.values())[0]
