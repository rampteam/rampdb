def is_numeric(lit):
    'Return value of numeric literal string or ValueError exception'

    # Handle '0'
    if lit == '0':
        return True
    # Int/Float/Complex
    try:
        int(lit)
        return True
    except ValueError:
        pass
    try:
        return float(lit)
    except ValueError:
        pass
    return False


def is_string(s):
    return isinstance(s, str)


def is_alphanumeric(s):
    return is_string(s) or is_numeric(s)
