def bytes_to_long(bytes):
    return int.from_bytes(bytes, 'little')