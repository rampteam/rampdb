#!/usr/bin/env python
import os
import shutil

start_port = 27018


def mongod_start(cmd):
    os.system(cmd)


def mongod_stop(cmd):
    cmd += " --shutdown"
    os.system(cmd)


def mongods_op(nr, op):
    cmd_f = "mongod --fork --logpath /var/log/mongolib/standalone/{}/{}.log\
     --dbpath /var/lib/mongolib/standalone/{} --port {}"
    port_ls = get_port_list(nr)
    for port in port_ls:
        cmd = cmd_f.format(port, port, port, port)
        op_func = globals()[op]
        op_func(cmd)


def stop_mongods(nr):
    pass


def dir_op(nr, op):
    port_ls = get_port_list(nr)
    types = ["log", "lib"]
    dir_format = "/var/{}/mongolib/standalone/{}"
    for dir_type in types:
        for port in port_ls:
            path = dir_format.format(dir_type, port)
            op_func = globals()[op]
            op_func(path)


def create_dir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def delete_dir(dir_path):
    if os.path.exists(dir_path):
        try:
            # if os.path.isfile(file_path):
            shutil.rmtree(dir_path)
        except Exception as e:
            raise


def get_port_list(nr):
    ls = []
    for el in range(0, nr):
        ls.append(str(start_port + el))

    return ls


def mongo_up(nr):
    dir_op(nr, "delete_dir")
    dir_op(nr, "create_dir")
    mongods_op(nr, "mongod_start")


def start_processes(nr):
    mongods_op(nr, "mongod_start")


def mongo_down(nr):
    mongods_op(nr, "mongod_stop")


def bootstrap(op):
    pass

# start recached
if __name__ == '__main__':
    mongo_up(2)
