import random


def random_from_list(data):
    return random.choice(data)


def pop_random_from_list(data):
    idx = random.randint(0, len(data) - 1)
    return data.pop(idx)
