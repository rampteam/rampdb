import time
from setproctitle import setproctitle
from ._abstract import AbstractDaemon
from ..config import settings
from ..errors import ConfigError, ExitError
from ..socketserver import RequestHandler, Server
from ..catalog_refresher import CatalogRefresher
from ..catalog import CatalogManagerFactory


class Daemon(AbstractDaemon):
    _should_run = True

    def __init__(self):
        log_file = settings.get('error_log')
        super().__init__(
            log_level=settings.get('log_level', 'INFO').upper(),
            stdout=log_file,
            stderr=log_file,
            pid_file=settings.get('pid_file', None),
            logger_name=settings.get('logger_name'),
        )

        self._catalog_refresher = CatalogRefresher()

    def run(self):
        setproctitle('CoordinatorDaemon')
        host = settings.get('client_host', '127.0.0.1', section='server')
        port = settings.get('client_port', section='server')
        if not port:
            raise ConfigError('Client port not found')

        self.logger.debug('Start refresher')

        # init catalog factory
        CatalogManagerFactory.inst(self.get_config_servers())
        self._catalog_refresher.start()

        port = int(port)
        self.server = Server((host, port), None, RequestHandler)
        while self.server.start_success is None:
            time.sleep(0.1)
        if not self.server.start_success:
            raise ExitError('Unable to start the server')
        self.server.serve_forever()

    def get_config_servers(self):
        """
        Returns a list of connection info dicts
        Used in target cluster instantiation

        """
        c_strings = settings.get('config_servers', section='cfg_server')
        ex = ConfigError("Cannot determine config servers!")
        if not c_strings:
            raise ex

        # now we try resolute connection data to split
        m_cstrings = list(
            filter(None, (x.strip() for x in c_strings.splitlines())))

        return m_cstrings

    def terminate(self):
        self.logger.debug('Terminate called')
        self.logger.debug('Terminate refresher')
        self._catalog_refresher.terminate()
        self._should_run = False
        self.server.shutdown()
        self.logger.debug('Terminate exit')
