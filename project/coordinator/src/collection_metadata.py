from .catalog import CatalogManagerFactory
from .model import Collection as CollectionModel


class CollectionMetadata(object):

    __instance = None

    def __init__(self):
        super(CollectionMetadata, self).__init__()
        self._name_to_info = {}
        self.reload()

    @classmethod
    def instance(cls):
        if not cls.__instance:
            cls.__instance = cls()
        return cls.__instance

    def reload(self):
        catalog_query = CatalogManagerFactory.inst().get_catalog_query()

        collections = catalog_query.by_example(
            CollectionModel.get_full_ns(),
            {}
        )

        for coll in collections:
            model = CollectionModel.from_dict(coll)
            self._name_to_info[model.full_name] = {
                "key": model.key,
                "unique": model.unique,
                "dropped": model.dropped,
                "updated_at": model.updated_at,
            }

    def get_key_for_ns(self, ns):
        if ns in self._name_to_info:
            return self._name_to_info[ns]['key']

    def get_key_for_ns_with_retry(self, ns):
        k = self.get_key_for_ns(ns)
        if k:
            return k
        self.reload()

        k = self.get_key_for_ns(ns)
        if k:
            return k

        raise ValueError("Unknown ns {}!".format(ns))
