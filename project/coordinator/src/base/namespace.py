def ns_is_db_only(name):
    s = name.split(".")
    if len(s) > 1:
        return False

    return True


def get_ns_parts(ns):
    return ns.split(".")


def get_db_from_ns(ns):
    parts = get_ns_parts(ns)
    if not parts:
        raise ValueError("Invalid ns {} for database!".format(ns))
    return parts[0]


def get_collection_from_ns(ns):
    parts = get_ns_parts(ns)
    if not len(parts) > 1:
        raise ValueError("Invalid ns {} for collection!".format(ns))

    return parts[1]
