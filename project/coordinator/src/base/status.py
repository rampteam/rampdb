from ..errors import CoordinatorError
from .error_codes import ErrorCodes


class Status(object):

    def __init__(self, err_code=0, reason="", location=""):
        super(Status, self).__init__()
        self._err_code = err_code
        self._reason = reason
        self._location = location

    @property
    def error_code(self):
        return self._err_code

    @property
    def reason(self):
        return self._reason

    @property
    def location(self):
        return self._location

    def is_ok(self):
        return self._err_code == 0

    def to_exception(self):
        msg = "Error Code:{} Reason: {}"
        return CoordinatorError(msg.format(self.error_code, self))

    @classmethod
    def OK(cls):
        return Status(0)

    @classmethod
    def from_ex(cls, ex):
        return cls(ErrorCodes.InternalError, str(ex))


    def get_ok_data(self):
        return {
            "status": "successfull"
        }

    def get_err_data(self):
        return {
            "status": "error",
            "error_code": self.error_code,
            "reason": self.reason,
            "location": self.location,

        }

    def get_data(self):
        if self.is_ok():
            return self.get_ok_data()

        return self.get_err_data()

    def __str__(self):
        msg = "Status ("
        msg += "err_code={}, "
        msg += "reason={}, "
        msg += "location={})"

        return msg.format(
            self.error_code,
            self.reason,
            self.location
        )
