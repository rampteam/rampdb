import time

from .catalog import CatalogManagerFactory
from .stop_thread import StopThread


class CatalogRefresher(StopThread):
    def run(self):
        self.logger.debug('Starting coordinator refresh thread.')
        self.catalog_data = CatalogManagerFactory.inst().get_catalog_data()
        while self._should_run:
            self.catalog_data.reload_if_changes()
            time.sleep(3)

    def terminate(self):
        super().terminate()
        self.logger.debug('Terminating coordinator refresh thread.')
        self._should_run = False
        self.catalog_data.stop_pooling()
        self.logger.debug('Terminating coordinator refresh finished.')
