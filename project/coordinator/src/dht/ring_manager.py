from ..catalog import CatalogManagerFactory
from .hash_ring import HashRing
from ..token_manager import TokenManager


class RingManager(object):

    __instance = None

    def __init__(self):
        self._catalog = CatalogManagerFactory.inst().get_catalog()
        self._catalog_data = CatalogManagerFactory.inst().get_catalog_data()
        self._token_manager = TokenManager.instance()

    @classmethod
    def instance(cls):
        if not cls.__instance:
            cls.__instance = cls()
        return cls.__instance

    def add_shard(self, name, host, max_size):
        # get all sharded collections
        sharded_ns = self._get_sharded_ns()

        # update hash rings for all namespaces
        for sh_ns in sharded_ns:
            hash_ring = HashRing(sh_ns)
            op = hash_ring.add_node(name)

            # update host
            op["meta"]["sh_names_to_host"][name] = host

            status = self._commit_and_update(op)
            if not status.is_ok():
                return status

        # finally add shard into db
        return self._catalog.add_shard(name, host, max_size)

    def _get_sharded_ns(self):
        sharded_colls = self._catalog_data.get_all_collections()
        sharded_ns = []
        for sh_coll in sharded_colls:
            sharded_ns.append(sh_coll["full_name"])

        return sharded_ns

    def _commit_and_update(self, op):
        # move tokens based on op
        status = self._token_manager.move_token_op(op)
        if not status.is_ok():
            return status

        # update token meta
        status = self._catalog.update_token_op(op)
        return status

    def remove_shard(self, name):
        # get all sharded collections
        sharded_ns = self._get_sharded_ns()

        # update hash rings for all namespaces
        for sh_ns in sharded_ns:
            hash_ring = HashRing(sh_ns)
            op = hash_ring.remove_node(name)

            status = self._commit_and_update(op)

            if not status.is_ok():
                return status
        # finally remove shard from db
        return self._catalog.remove_shard(name)
