from ..shards import ShardInfo
from ..token_metadata import TokenMetadata
from .partitioner.factory import Factory as PartitionerFactory
from ..model import Token
from ..util import randomify


class HashRing(object):
    # Number of ring partitions
    __NR_PARTITIONS = 30  # 2 ** 10

    def __init__(self, ns):
        super(HashRing, self).__init__()
        self._ns = ns
        self._tokens_info = TokenMetadata(self._ns)

        # Delete
        self._tokens_info.reload()

    @property
    def ns(self):
        return self._ns

    def get_tokens_per_node(self, sh_count, round=True):
        rounded = self.__NR_PARTITIONS // sh_count
        if round:
            return rounded
        else:
            remainder = self.__NR_PARTITIONS - (rounded * sh_count)
            return (rounded, remainder)

    def get_current_tokens_per_node(self, sh_count):
        sh_count = self._get_shards_count()
        return self.get_tokens_per_node(sh_count)

    def _get_shards_count(self):
        sh_info = self._get_shards_info_provider()
        return sh_info.shards_count()

    def add_node(self, node):

        # TODO remove this
        self._reload()

        # init node case
        if not self._get_shards_count():
            return self.init_ring([node])

        new_sh_count = self._get_shards_count() + 1
        new_tokens_per_node = self.get_tokens_per_node(new_sh_count)

        token_metadata = self._get_token_info_provider()
        # token_metadata.reload()
        shards_to_tokens = token_metadata.shards_to_tokens

        move_data = {}
        nr_moved_tokens = 0
        for sh, tokens in shards_to_tokens.items():

            # check boundary we do not want to move more than necessary
            if nr_moved_tokens >= new_tokens_per_node:
                break

            nr_to_move = len(tokens) - new_tokens_per_node

            # special case check if nr_to_move > new_tokens_per_node
            if nr_to_move > new_tokens_per_node:
                nr_to_move = new_tokens_per_node
            nr_moved_tokens += nr_to_move

            if not nr_to_move:
                continue

            to_move = set()
            while (len(to_move) < nr_to_move):
                to_move.add(randomify.random_from_list(tokens))

            move_data[sh] = list(to_move)

        return {
            "target": node,
            "op": "move_to_set",
            "meta": self.get_meta(),
            "data": move_data
        }

    def get_meta(self):
        d = self._get_shards_info_provider().get_names_to_host()
        return {
            "sh_names_to_host": d
        }

    def remove_node(self, node):

        # TODO remove this
        self._reload()

        token_metadata = self._get_token_info_provider()
        tokens_to_move = token_metadata.get_tokens_for_shard(node)

        shard_info = self._get_shards_info_provider()
        all_shards = shard_info.get_shard_names()

        move_data = {}

        i = 0
        len_tokens = len(tokens_to_move)
        while i < len_tokens:
            for sh in all_shards:
                # avoid current removed shard
                if sh == node:
                    continue

                if not i < len_tokens:
                    break
                if sh not in move_data:
                    move_data[sh] = set()
                move_data[sh].add(tokens_to_move[i])
                i += 1

        return {
            "target": node,
            "op": "move_to_remove",
            "meta": self.get_meta(),
            "data": move_data
        }

    def init_ring(self, nodes: list):
        nr_nodes = len(nodes)
        tokens = self.get_token_partitions()
        if nr_nodes == 1:
            return {
                "target": nodes[0],
                "op": "set",
                "meta": self.get_meta(),
                "data": tokens
            }

        rounded, remainder = self.get_tokens_per_node(nr_nodes, round=False)

        data = []
        for node in nodes:
            to_add = rounded
            d = {
                "target": node,
                "op": "set",
                "data": []
            }
            if remainder:
                to_add += 1
                remainder -= 1

            for i in range(to_add):
                token = randomify.pop_random_from_list(tokens)
                d["data"].append(token)

            data.append(d)

        return data

    def get_token_partitions(self):
        partitions = []
        partitioner = self._get_partitioner()

        # Warning assumption nr partitions power of 2
        # TODO check formula
        min_key = partitioner.get_minimum().key
        max_key = partitioner.get_maximum().key

        partition_size = (abs(min_key) + abs(max_key)) // self.__NR_PARTITIONS

        mn_val = min_key
        for i in range(1, (self.__NR_PARTITIONS + 1)):
            if i > 0:
                mn_val = min_key + (i - 1) * partition_size

            mx_val = min_key + i * partition_size

            # in case of nr patitions not divisible by 2
            if i == self.__NR_PARTITIONS:
                mx_val = mn_val + (max_key - mn_val)
            name = "token" + str(i)

            # print(mn_val)
            # print(mx_val)
            # print("\n")
            token = Token(
                name=name,
                ns=self.ns,
                min=mn_val,
                max=mx_val
            )

            partitions.append(token)

        return partitions

    def _reload(self):
        self._get_shards_info_provider().reload()
        self._get_token_info_provider().reload()

    def _get_shards_info_provider(self):
        return ShardInfo.instance()

    def _get_token_info_provider(self):
        return self._tokens_info

    def _get_partitioner(self):
        return PartitionerFactory.get_partitioner()
