import mmh3

from .partitioner import Partitioner
from ..key_factory import KeyFactory
import random


class LongKey(object):

    def __init__(self, key):
        self.__key = key

    def to_string(self):
        return str(self.__key)

    @classmethod
    def from_bytes(cls, by):
        return cls(int.from_bytes(by, 'little'))

    def to_bytes(self):
        return self.key.to_bytes((self.key.bit_length() // 8) + 1, byteorder='little')

    @property
    def key(self):
        return self.__key

    def __hash__(self, ):
        return hash(self.__key)

    def __eq__(self, other):
        if other == self:
            return True
        if not other:
            return False
        if not isinstance(other, LongKey):
            return False
        return self.key == other.key

    def __str__(self):
        return "LongKey({})".format(self.to_string())

    def __ge__(self, other):
        return self.key >= other.key

    def __gt__(self, other):
        return self.key >= other.key

    def __lt__(self, other):
        return self.key < other.key

    def __le__(self, other):
        return self.key <= other.key


class Murmur3(Partitioner):

    def __init__(self):
        super(Murmur3, self).__init__()
        self.__factory = MKeyFactory()
        self.__space_len = 63

    def get_random_token(self):
        by = random.getrandbits(self.__space_len)
        return self.__factory.from_byte_array(by)

    def key_factory(self):
        return self.__factory

    def midpoint(self, left: LongKey, right: LongKey):
        if left < right:
            m = (left.key + right.key) // 2
        else:
            # length of range we're bisecting is (R - min) + (max - L)
            # so we add that to L giving
            # L + ((R - min) + (max - L) / 2) = (L + R + max - min) / 2
            mx = self.get_maximum().key
            mn = self.get_minimum().key
            m = (mx - mn + left.key + right.key) // 2
            if m > mx:
                m += m - mx

        return LongKey(m)

    def get_minimum(self):
        return LongKey(-2 ** self.__space_len + 1)

    def get_maximum(self):
        return LongKey(2 ** self.__space_len - 1)


class MKeyFactory(KeyFactory):

    def from_byte_array(self, bytes):
        return LongKey.from_bytes(bytes)

    def to_string(self, key):
        return key.to_string()

    def to_byte_array(self, key):
        return key.to_bytes()

    def from_long(self, key):
        return LongKey(key)

    def from_string(self, string):
        # by = mmh3.hash_bytes(string)
        # return self.from_byte_array(by)

        by = mmh3.hash(string)
        if isinstance(by, list):
            return LongKey(by[0])

        return LongKey(by)
