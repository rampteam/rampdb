from .murmur3 import Murmur3
from ...config import settings


class Factory(object):

    @classmethod
    def get_partitioner(cls):
        p = settings.get('partitioner', section='sharding')
        if p == 'murmur':
            return cls.get_murmur()
        raise ValueError("Unknown partitioner: {}".format(p))

    @classmethod
    def get_key_factory(cls):
        return cls.get_partitioner().key_factory()

    @classmethod
    def get_murmur(cls):
        return Murmur3()
