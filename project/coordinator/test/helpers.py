from src.catalog.factory import CatalogManagerFactory
from src.shards import ShardInfo
from src.token_metadata import TokenMetadata
from src.factory import CoordinatorFactory
from src.dbclient.connection import Connection
from src.dbclient.connection_string import ConnectionString
from src.dht.hash_ring import HashRing
from .mock import *
from src.commands import *


def get_cfg_servers():
    return ["127.0.0.1:7000"]


def get_catalog_manager():
    CatalogManagerFactory.inst(get_cfg_servers())
    return CatalogManagerFactory.inst().get_catalog()


def get_collection_metadata():
    m = CoordinatorFactory.get_collection_metadata()
    m.reload()
    return m


def get_token_metadata():
    m = TokenMetadata("db.sh")
    m.reload()
    return m


def ns():
    return "db.sh"


def get_conn_for_host(host):
    conn_string = ConnectionString.from_text(host)
    return Connection.from_conn_string(conn_string)


def get_shard_info():
    s = ShardInfo.instance()
    s.reload()
    return s


def init_hash_ring(sh="shard1"):
    h = HashRing("db.sh")
    c = get_catalog_manager()
    data = h.init_ring([sh])
    c.update_token_op(data)


def add_shard(sh="shard2"):
    h = HashRing("db.sh")
    op = h.add_node("shard2")
    c = get_catalog_manager()
    # print(op)
    c.update_token_op(op)


def remove_shard(sh="shard1"):
    h = HashRing("db.sh")
    op = h.remove_node("shard1")
    c = get_catalog_manager()
    print(op)
    # c.update_token_op(op)


def reset_hash_ring():
    conn = get_conn_for_host("127.0.0.1:7000")
    coll = conn.select_collection("config", "tokens")
    coll.remove({})


def get_coord_conn():
    return get_conn_for_host("127.0.0.1:1111")


def get_coord_test_coll():
    conn = get_coord_conn()
    return conn.select_collection("db", "sh")


def get_coord_test_db():
    conn = get_coord_conn()
    return conn.select_database("db")


def insert_names():
    names = get_random_names()
    for name in names:
        options = {
            "docs": {"name": name},
        }

        insert = InsertCmd(ns(), options)
        insert.run()


def test_cluster():
    conn = get_conn_for_host("127.0.0.1:7001")
    coll = conn.select_collection("db", "sh")
    coll.insert({"bla1": 1})

    conn = get_conn_for_host("127.0.0.1:7000")
    coll = conn.select_collection("db", "tst")
    coll.insert({"bla1": 1})
