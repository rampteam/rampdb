from src.dht.partitioner.factory import Factory
from .helpers import *
from src.model import *
from src.dbclient.connection_string import ConnectionString, HostAndPort
from src.dht.hash_ring import HashRing
from src.dht.ring_manager import RingManager
from src.shards import ShardInfo
from src.token_metadata import TokenMetadata
from src.factory import CoordinatorFactory
from src.commands import *
from .mock import *
from src.migrate import *
from src.distributed_lock import DistributedLock
from src.catalog_refresher import CatalogRefresher
from collections import OrderedDict


def test_partitioner():
    m = Factory.get_murmur()

    print(m.get_minimum())
    print(m.get_maximum())

    f = m.key_factory()
    l = f.from_string("bla")
    print(l)


def test_error_codes():
    pass


def test_refresher():
    r = CatalogRefresher()
    r.run()
    r.join()


def test_catalog():
    c = get_catalog_manager()
    # c.insert(Database.get_full_ns(), {"name": "check"})

    # data = c.by_example(Database.get_full_ns(), {"name": "test"})
    # print(data)

    # c.update(Database.get_full_ns(), {"name": "test"}, {"name": "updated"}, False, False)

    # c.remove(Database.get_full_ns(), {"name": "test"})
    # enable sharding
    options = {
        "key": {
            "name": "hashed"
        }

    }
    c.shard_collection("db.sh", options)

    # add shard
    # conn_string = ConnectionString.from_text("127.0.0.1:7000")
    # c.add_shard("shard1", conn_string, 4800)
    # c.add_shard("shard2", conn_string, 4800)


def test_error_codes():
    from src.base import Status, ErrorCodes

    s = Status(ErrorCodes.InternalError, "bla")
    print(s)


def test_ring_manager():
    ring = RingManager.instance()
    # host = "127.0.0.1:7000"
    # ring.add_shard("shard1", host, 4800)

    host = "127.0.0.1:7001"
    ring.add_shard("shard2", host, 4800)

    # ring.remove_shard("shard2")
    # ring.remove_shard("shard1")


def test_hash_ring():
    h = HashRing("db.sh")
    # print(h.get_tokens_per_node(2))
    # print(h.get_token_partitions())
    n = ["sh1", "sh2", "sh3"]
    # for x in range(1,10):
    # print(x)
    # print(h.get_tokens_per_node(x, False))
    #     print("\n")
    #d = h.init_ring(n)
    # print(d)
    c = get_catalog_manager()
    sh_info = get_shard_info()
    sh1 = sh_info.find_by_name_with_retry("shard1")
    sh2 = sh_info.find_by_name_with_retry("shard2")
    # m = get_token_metadata()
    # t = m.get_tokens_for_shard(sh2)
    # print(t)
    # return

    #data = h.init_ring(["shard1"])
    # c.update_token_op(data)
    op = h.add_node("shard2")
    c.update_token_op(op)
    # sh_info = h._get_token_info_provider()._get_shard_info()
    # sh11 = sh_info .find_by_name_with_retry("shard1")
    # t = h._get_token_info_provider()
    # print(hash(sh1))
    # print(hash(sh11))
    # print(sh1 == sh11)
    # print(sh11)
    # print(t._shards_to_tokens)
    # print(t.get_tokens_for_shard(sh1))
    # return
    # data = h.remove_node("shard1")
    # print(data)
    #status = c.update_token_op(data)
    # print(status)


def test_shard_info():
    c = get_catalog_manager()
    si = ShardInfo.instance()
    si.reload()
    shards = si.get_all_shards()
    # print(shards)
    # print(si.get_shard_names())


def test_token_metadata():
    m = get_token_metadata()
    print(m.get_all_tokens())


def find_cmd():
    options = {
        "ns": "db.sh"
    }
    fnd = FindCmd(ns(), options)
    data = fnd.run()
    print(data)


def update_cmd():
    options = {
        "ns": "db.sh",
        "query": {},
        "update": {
            "$set": {"updated": True}
        }
    }
    update = UpdateCmd(ns(), options)
    data = update.run()
    print(data)


def remove_cmd():
    options = {
    }
    rm = RemoveCmd(ns(), options)
    data = rm.run()
    print(data)


def init_hash_ring():
    get_shard_info()
    # init_hash_ring()
    # add_shard()
    pass


def test_lock():
    lock = DistributedLock()
    lock.busy_acquire(
        ns(),
        "token_balancer",
        "balance ring token"
    )
    lock.release()


def test_migrate():
    options = {
        "full_ns": ns(),
        "from_shard": "127.0.0.1:7000",
        "to_shard": "127.0.0.1:7001",
        "min": -9223372036854775807,
        "max": 1844674407370955153
    }

    mv = MoveTokenCommand(options)

    mv.run()


def get_coord():
    conn = get_conn_for_host("127.0.0.1:1111")
    return conn.coordinator()


def ctest_add_shard():
    conn = get_conn_for_host("127.0.0.1:1111")
    coord = conn.coordinator()
    # host = "127.0.0.1:7000"
    # res = coord.add_shard("shard1", host, 4800)

    # host = "127.0.0.1:7001"
    # res = coord.add_shard("shard2", host, 4800)


    # res = coord.remove_shard("shard2")
    res = coord.remove_shard("shard1")
    print(res)


def ctest_insert():
    coll = get_coord_test_coll()
    names = get_random_names()
    docs = []
    for name in names:
        docs.append({"name": name})

    res = coll.insert(docs)
    print(res)

def ctest_remove():
    coll = get_coord_test_coll()
    
    coll.remove({})
    # coll.remove({"name": "Jayne"})

def ctest_update():
    coll = get_coord_test_coll()
    res = coll.update(
        {"name": "Jayne"}, 
        {"$set": {"updated": True}},
        upsert=False,
        multi=True
    )

    print(res)

def ctest_find():
    coll = get_coord_test_coll()
    res = coll.by_example(
        {"name": "Jayne"}, 
    )

    print(res)

def ctest_cindex():
    coll = get_coord_test_coll()
    keys = OrderedDict([("name", 1)])
    res = coll.create_index("index_tst", keys)

    print(res)

def ctest_coll_names():
    db = get_coord_test_db()
    res = db.collection_names()
    print(res)


def ctest_coll_op():
    db = get_coord_test_db()
    res = db.drop_collection("sh")
    print(res)


def ctest_coord_client():
    conn = get_conn_for_host("127.0.0.1:1111")
    coord = conn.coordinator()
    print(coord)


def ctest_sharding():
    conn = get_conn_for_host("127.0.0.1:1111")
    coord = conn.coordinator()
    options = {
        "key": {
            "name": "hashed"
        }
    }

    res = coord.shard_collection("db.sh", options)

    print(res)
    

def main():
    # c = get_catalog_manager()
    # m = get_token_metadata()
    # get_collection_metadata()
    # get_shard_info()
    # ctest_sharding()
    ctest_add_shard()
    # ctest_coll_op()
    # ctest_find()
    # ctest_remove()
    # ctest_insert()
    # update_cmd()
    # ctest_update()
    # test_refresher()
    # ctest_insert()
    # test_ring_manager()
    # test_catalog()
    # insert_names()
    # test_migrate()
    # test_lock()
    # remove_shard()
    # test_catalog()
    # test_hash_ring()
    # test_shard_info()
    # test_token_metadata()
    # find_cmd()
    # test_cluster()
    # update_cmd()
    # remove_cmd()
    # insert_names()
    # test_error_codes()

