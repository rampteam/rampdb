from setuptools import setup, find_packages

setup(
    name='coordinator',
    version='0.0.1',
    packages=find_packages(),
    url='',
    license='',
    author='robert maftei & alex porcescu',
    author_email='',
    description='',
    install_requires=[
        'click',
        'mmh3',
        'pytz',
        'python-dateutil',
        'pymongo',
        'setproctitle'
    ],
    entry_points={
        'console_scripts': [
            'coordinator = cli:main'
        ]
    }
)
