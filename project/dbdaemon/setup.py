from setuptools import setup, find_packages

setup(
    name='dbdaemon',
    version='0.0.1',
    packages=find_packages(),
    url='',
    license='',
    author='robert maftei & alex porcescu',
    author_email='',
    description='',
    install_requires=[
        'click',
        'pymongo',
        'setproctitle',
    ],
    entry_points={
        'console_scripts': [
            'dbdaemon = cli:main'
        ]
    }
)
