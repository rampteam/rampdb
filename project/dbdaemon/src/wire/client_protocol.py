import bson


class ClientProtocol(object):
    def send(self, name, params):
        params["c"] = name
        return bson.BSON.encode(params)
