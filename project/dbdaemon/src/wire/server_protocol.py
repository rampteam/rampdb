import logging
import traceback

from bson import BSON

from .database import DbFactory, is_commit_command
from .errors import DuplicateKeyError
from ..errors import InvalidCommandError, InvalidName
from ..config import settings
from ..commit_log import commit_log
from ..replication import cluster
from ..replication.errors import ClusterNotInitialized, MasterCommandError


def raw_data_setter(f):
    def wrapper_f(*args):
        setattr(args[0], 'raw_data', args[1])
        response = f(*args)
        setattr(args[0], 'raw_data', None)
        return response

    return wrapper_f


def commit_log_decorator(f):
    def wrapper_f(*args):
        if is_commit_command(args[1].get('type'), args[1].get('command')):
            if cluster.is_cluster:
                if not cluster.is_active:
                    raise ClusterNotInitialized('Cluster is not initialized')
                if not cluster.this_is_master:
                    params = args[1].get('params', {})
                    if args[1].get('command') == 'replicate' and params.get(
                            'id') == cluster.replica_id:
                        self = args[0]
                        for line in params['raw_data']:
                            command = ServerProtocol.get_decoded_data(line[1])
                            handler = getattr(self, 'handle_{}'.format(command['type']), None)
                            if callable(handler):
                                try:
                                    handler(command)
                                    commit_log.write(line)
                                except DuplicateKeyError as e:
                                    print('DUPLICATE ERROR: {}'.format(str(e)))
                                    print(line)
                            else:
                                raise ValueError(
                                    "No handler found for {} {}".format(
                                        command['type'],
                                        command['command']
                                    )
                                )

                        return 'Replication success'
                    raise MasterCommandError(cluster.master_host)
            response = f(*args)
            commit_log.write(getattr(args[0], 'raw_data'))
        else:
            response = f(*args)
        return response

    return wrapper_f


class ServerProtocol(object):
    __decoder = None
    raw_data = None

    def __init__(self):
        self.db_factory = DbFactory.get_specific_factory()

    @staticmethod
    def get_decoded_data(data):
        data = BSON.decode(data)
        meta = data.get('c').split('.')
        if len(meta) != 2:
            raise InvalidCommandError('Invalid command: {}'.format(data.get('c')))
        return {
            'type': meta[0],
            'command': meta[1],
            'params': data.get('p')
        }

    @raw_data_setter
    def data_received(self, data):
        try:
            response = {'ok': 1, 'data': self.handle(self.get_decoded_data(data))}
        except Exception as e:
            logger = logging.getLogger(settings.get('logger_name'))
            logger.warning(traceback.format_exc())
            response = {'ok': 0, 'error': str(e).encode()}
        return response

    @commit_log_decorator
    def handle(self, command: dict):
        handler = getattr(self, 'handle_{}'.format(command['type']), None)
        if callable(handler):
            return handler(command)
        else:
            raise ValueError(
                "No handler found for {} {}".format(command['type'], command['command']))

    def handle_c(self, command: dict):
        db, coll = self._get_ns(command['params'].get('ns'))

        if command['command'][0] == '_':
            raise InvalidCommandError('Command unknown: {}'.format(command['command']))

        collection = self.db_factory.collection(db, coll)
        coll_cmd = getattr(collection, command['command'], None)

        if not callable(coll_cmd):
            raise InvalidCommandError('Command {} is invalid'.format(command['command']))
        return coll_cmd(command['params'])

    def handle_d(self, command):
        db_name = command['params'].get('ns')

        if command['command'][0] == '_':
            raise InvalidCommandError('Command unknown: {}'.format(command['command']))

        db = self.db_factory.database(db_name)
        db_cmd = getattr(db, command['command'], None)

        if not callable(db_cmd):
            raise InvalidCommandError('Command {} is invalid'.format(command['command']))
        return db_cmd(command['params'])

    def handle_s(self, command):
        if command['command'][0] == '_':
            raise InvalidCommandError('Command unknown: {}'.format(command['command']))

        db = self.db_factory.cluster()
        db_cmd = getattr(db, command['command'], None)

        if not callable(db_cmd):
            raise InvalidCommandError('Command {} is invalid'.format(command['command']))
        return db_cmd(command['params'])

    def _get_ns(self, ns: str):
        if not isinstance(ns, str):
            raise InvalidName('Database and collection name must be a string')
        db, coll = ns.split('.', 1)
        self._coll_validate(coll)
        return db, coll

    def _coll_validate(self, string: str):
        if not string or '..' in string:
            raise InvalidName('Collection name cannot be empty')
        if '$' in string:
            raise InvalidName('Invalid character \'$\' in {}'.format(string))
        if string[0] == '.' or string[-1] == '.':
            raise InvalidName('Collection cannot start or end with \'.\'')
        if '\x00' in string:
            raise InvalidName('Collection cannot contain null character')
