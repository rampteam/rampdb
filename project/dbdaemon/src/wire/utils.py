import random


def get_bit(byteval, idx):
    return int(((byteval & (1 << idx)) != 0))


def set_bit(value, bit):
    return value | (1 << bit)


def clear_bit(value, bit):
    return value & ~(1 << bit)


def get_random_bits(nr):
    return random.getrandbits(nr)
