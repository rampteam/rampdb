import pymongo

from .mongodb import Collection as MongodbCollection, Database as MongodbDatabase
from ..cluster_commands import ClusterCommands
from ...config import settings


class DbFactory:
    @staticmethod
    def get_specific_factory():
        db_type = settings.get('type', section='database')
        if db_type == 'mongodb':
            return MongodbFactory()
        raise ValueError('No factory for database: {}'.format(db_type))


class BaseFactory:
    def cluster(self):
        return ClusterCommands()


class MongodbFactory(BaseFactory):
    _conn = None

    def __init__(self):
        self._conn = pymongo.MongoClient(settings.get('connection', section='database'))

    def collection(self, db: str, collection: str):
        coll = self._conn[db][collection]
        return MongodbCollection(coll)


    def database(self, db: str):
        db = self._conn[db]
        return MongodbDatabase(db)
