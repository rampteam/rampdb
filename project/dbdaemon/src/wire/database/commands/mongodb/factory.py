from .recv_move_token import RecvMoveToken
class CommandFactory(object):

    @classmethod
    def get_command_handler(cls, cmd, options):
        if cmd == 'recv_move_token':
            return RecvMoveToken(options)

        raise ValueError("Unknown command handler for cmd: {}".format(cmd))


