from pymongo import MongoClient


class RecvMoveToken(object):
    def __init__(self, options):
        self._full_ns = options.get("full_ns", None)
        self._from_shard = options.get("from_shard", None)
        self._to_shard = options.get("to_shard", None)
        self._query = options.get("query", None)
        self._sync_from = options.get("sync_from", None)

    def run(self):

        # Find and insert docs from target
        from_coll = self.get_from_collection()
        docs = from_coll.by_example(self._query)

        to_coll = self.get_to_collection()
        response = to_coll.insert(docs)

        check = ["writeError", "writeConcernError"]
        for ch in check:
            data = response.get(ch, None)
            if data is not None:
                raise Exception("Insert move token failed! Error: {}".format(data))

        return response

    def get_from_collection(self):
        conn_string = MongoClient(self._from_shard)
        db, coll = self._get_ns(self._full_ns)
        return conn_string[db][coll]

    def get_to_collection(self):
        conn_string = MongoClient(self._to_shard)
        db, coll = self._get_ns(self._full_ns)
        return conn_string[db][coll]

    def _get_ns(self, ns: str):
        if not isinstance(ns, str):
            raise Exception('Database and collection name must be a string')
        db, coll = ns.split('.', 1)
        return db, coll