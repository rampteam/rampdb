from pymongo import ASCENDING, DESCENDING
from pymongo.database import Database as MongoDatabase
from pymongo.collection import Collection as MongoCollection
from pymongo.errors import DuplicateKeyError as MongoDuplicateKeyError
from ...errors import UpdateError, RemoveError, CreateIndexError
from .commands.mongodb.factory import CommandFactory
from ..errors import DuplicateKeyError
from .commit_commands import commit_mark

coll_commit_mark = commit_mark('c')


class Collection:
    def __init__(self, coll: MongoCollection):
        self.coll = coll

    @coll_commit_mark
    def insert(self, params: dict):
        try:
            return self.coll.insert(params.get('docs'))
        except MongoDuplicateKeyError as e:
            raise DuplicateKeyError(str(e))

    def query(self, params: dict):
        result = self.coll.find(
            limit=params.get('n_to_return', 0),
            skip=params.get('n_to_skip', 0),
            projection=params.get('fields'),
            filter=params.get('query')
        )
        return [x for x in result]

    @coll_commit_mark
    def remove(self, params):
        result = self.coll.remove(
            params.get('spec'),
            multi=not params.get('single', False)
        )
        if not result.get('ok'):
            raise RemoveError(result.get('err'))
        del result['ok']
        return result

    @coll_commit_mark
    def update(self, params):
        result = self.coll.update(
            spec=params.get('query'),
            document=params.get('update'),
            multi=params.get('multi', False),
            upsert=params.get('upsert', False)
        )
        if not result.get('ok'):
            raise UpdateError(result.get('err'))
        del result['ok']
        return result

    @coll_commit_mark
    def rename_collection(self, params: dict):
        return self.coll.rename(params.get('new'))

    @coll_commit_mark
    def drop_index(self, params: dict):
        return self.coll.drop_index(params.get('identifier'))

    @coll_commit_mark
    def create_index(self, params: dict):
        keys = []
        for k, v in params.get('keys', {}).items():
            if int(v) == 1:
                keys.append((k, ASCENDING))
            elif int(v) == -1:
                keys.append((k, DESCENDING))
            else:
                raise CreateIndexError('Invalid index direction \'{}\' for key {}'.format(v, k))
        return self.coll.create_index(
            key_or_list=keys,
            name=params.get('name'),
            **params.get('options', {})
        )


db_commit_mark = commit_mark('d')


class Database:
    db = None

    def __init__(self, database: MongoDatabase):
        self.db = database

    def collection_names(self, params: dict):
        return self.db.collection_names(
            include_system_collections=params.get('include_system_collections', True)
        )

    @db_commit_mark
    def drop_collection(self, params: dict):
        return self.db.drop_collection(params.get('name'))

    @db_commit_mark
    def create_collection(self, params: dict):
        return self.db.create_collection(name=params.get('name'), **params.get('options', {}))

    def command(self, params: dict):
        name = params.get('name')
        # TODO add validation
        handler = CommandFactory.get_command_handler(name, params)
        return handler.run()

