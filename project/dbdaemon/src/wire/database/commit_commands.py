__COMMIT_COMMANDS = {
    's': set()
}
__COMMIT_COMMANDS['s'].add('replicate')


def is_commit_command(type, command):
    return command in __COMMIT_COMMANDS.get(type, set())


def commit_mark(command_type: str):
    cmd_dict = __COMMIT_COMMANDS.setdefault(command_type, set())

    def cm(f):
        def wrapper_f(*args):
            return f(*args)

        cmd_dict.add(f.__name__)
        return wrapper_f

    return cm
