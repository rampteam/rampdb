from time import sleep, monotonic

from ..config import settings
from ..replication import cluster, errors


class ClusterCommands:
    def add_slave(self, params: dict):
        host = params['hostname']
        cluster.add_new_server(host)

        start = monotonic()
        while not cluster.new_servers[host]['initial_connection']:
            sleep(0.1)
            if monotonic() - start > self._connect_timeout:
                raise Exception('Connection timeout: {} s'.format(self._connect_timeout))
        conn_data = cluster.new_servers[host]
        del cluster.new_servers[host]
        if not conn_data['connected']:
            raise Exception(conn_data['error'])
        return 'Connection successful'

    def set_as_master(self, params: dict):
        cluster.set_this_as_master(params['host'])
        return 'Master initiated'

    def cluster_status(self, params: dict):
        return cluster.status

    @property
    def _connect_timeout(self):
        return float(settings.get('replica_connect_timeout', section='replication'))