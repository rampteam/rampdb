import struct
from time import monotonic
from src.errors import SocketRecvError, SocketTimeoutError, SocketError

from ..config import settings


def receive(sock):
    bytes_to_receive = _receive(sock, 4)
    bytes_to_receive = struct.unpack('i', bytes_to_receive)[0]
    return _receive(sock, bytes_to_receive)


def _receive(sock, bytes_to_receive) -> bytes:
    response = b''
    bytes_remaining = bytes_to_receive
    receive_timeout = float(settings.get('receive_timeout', default=1, section='server'))
    while True:
        start = monotonic()
        recv_len = min(bytes_remaining, 1024)
        result = sock.recv(recv_len)
        if result:
            response += result
            bytes_remaining -= len(result)
            if not bytes_remaining:
                break
            if bytes_remaining < 0:
                raise SocketRecvError('Protocol error, invalid bytes received')
        else:
            # nothing was read, socket is dead or the buffer is empty
            if len(response) != bytes_to_receive:
                raise SocketRecvError(
                    'Message length error, expected: {}, received: {}'.format(bytes_to_receive,
                                                                              len(response)))
            break
        if monotonic() - start > receive_timeout:
            raise SocketTimeoutError
    return response


def send(sock, message: bytes):
    msg_len = struct.pack('I', len(message))
    message = msg_len + message
    last_index = 0
    try:
        while last_index < len(message):
            bytes_written = sock.send(message[last_index:])
            if bytes_written:
                last_index += bytes_written
            else:
                raise SocketError('Socket connection broken')
    except Exception as e:
        raise SocketError(str(e))
