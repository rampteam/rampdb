from socket import (
    socket,
    AF_INET,
    SOCK_STREAM,
)
from ._communication import send, receive


class Client:
    timeout = 30

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.sock:
            self.sock.close()

    def __init__(self, host, port, timeout=30):
        self.conn = (host, port)
        self.timeout = timeout
        self.sock = None

    def send(self, message: bytes):
        self._connect()
        return self._send(message)

    def _connect(self):
        if not self.sock:
            self.sock = socket(AF_INET, SOCK_STREAM)
            self.sock.settimeout(self.timeout)
            self.sock.connect(self.conn)

    def _send(self, message: bytes):
        send(self.sock, message)
        return receive(self.sock)
