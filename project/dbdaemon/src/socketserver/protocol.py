import asyncio
import logging
import struct
import traceback

from bson import BSON

from ..config import settings
from ..pool import acquire_object

pool = None


class ServerReceiveProtocol(asyncio.Protocol):
    __logger = None
    _message = b''
    _handler = None
    _loop = None
    commit_log = None
    connected_verified = False
    slave_id = None

    def __init__(self, *args, **kwargs):
        super(*args, **kwargs)
        self._loop = asyncio.get_event_loop()

    def connection_made(self, transport):
        self.transport = transport

    def connection_lost(self, exc):
        pass

    def data_received(self, data):
        self._message += data
        if len(self._message) > 4:
            try:
                bytes_to_receive = struct.unpack('i', self._message[0:4])[0]
                if len(self._message[4:]) != bytes_to_receive:
                    return
                # noinspection PyTypeChecker
                message = self._message[4:]
                self._message = b''
                asyncio.async(self.handle_response(message))
            except Exception as e:
                self.logger.warning(str(e))
                asyncio.async(self.write({'ok': 0, 'error': str(e)}))
                self.transport.close()

    @asyncio.coroutine
    def handle_response(self, message: bytes):
        try:
            with acquire_object(pool) as db:
                response = yield from self._loop.run_in_executor(None, db.data_received, message)
                asyncio.async(self.write(response))
        except Exception as e:
            yield from self._loop.run_in_executor(None, self.logger.warning, traceback.format_exc())
            yield from self._loop.run_in_executor(None, self.logger.warning, str(e))
            asyncio.async(self.write({'ok': 0, 'error': str(e)}, True))

    @asyncio.coroutine
    def write(self, data: dict, with_exit=False):
        message = BSON.encode(data)
        msg_len = struct.pack('I', len(message))
        message = msg_len + message
        self.transport.write(message)
        if with_exit:
            self.transport.close()

    def eof_received(self):
        self.transport.close()

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger
