import os


def reverse_read(filename: str, encoding: str='UTF-8', line_ending: bytes=b'\r\n'):
    """
    read last line from a file
    :param filename: file to read
    :param encoding: the encoding of the file
    :return:
    """
    with open(filename, 'rb') as f:
        offset = 0
        read_buffer_size = 10000

        buffer = b''

        buffer_lines = []

        f.seek(0, os.SEEK_END)
        current_line_start_offset = total_size = remaining_size = f.tell()
        first_iteration = True

        while True:
            buffer_lines_len = len(buffer_lines)
            for buffer_index in range(buffer_lines_len - 1, -1, -1):
                line = buffer_lines[buffer_index]
                current_line_start_offset -= (len(line) + len(line_ending))
                yield current_line_start_offset, line

            if remaining_size <= 0:
                break

            offset = min(total_size, offset + read_buffer_size)
            f.seek(-offset, os.SEEK_END)

            new_buffer = f.read(min(remaining_size, read_buffer_size))

            remaining_size -= read_buffer_size
            buffer_lines = (new_buffer + buffer).split(line_ending)

            if first_iteration:
                buffer_lines.pop()
                first_iteration = False

            if remaining_size > 0:
                buffer = buffer_lines.pop(0)