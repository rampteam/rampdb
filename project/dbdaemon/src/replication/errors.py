import json
from ..errors import Error


class ReplicaClusterError(Error):
    code = 100


class ServerAlreadyAdded(ReplicaClusterError):
    code = 101


class ClusterAlreadyActive(Error):
    code = 102


class ClusterNotInitialized(Error):
    code = 103


class ClusterProtocolError(Error):
    code = 104


class MasterCommandError(Error):
    def __str__(self):
        if self._message:
            return json.dumps({
                'error': self.__class__.__name__,
                'host': self._message,
            })
        return super().__str__()


class SlaveCommandError(Error):
    code = 106
