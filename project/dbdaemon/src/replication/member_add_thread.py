import traceback

from bson import BSON
from time import sleep
from uuid import uuid4

from .cluster import cluster as cluster
from .errors import ClusterProtocolError
from ..stop_thread import StopThread
from ..socketserver import Client


class MemberAddThread(StopThread):
    def run(self):
        while self._should_run:
            if not cluster.new_servers_exist():
                sleep(0.5)
                continue

            servers = cluster.new_servers
            for host, status in servers.items():
                try:
                    if status['initial_connection']:
                        continue
                    server_message = {
                        'command': 'add_to_cluster',
                        'id': uuid4().hex,
                        'name': cluster.name,
                        'master_host': cluster.master_host,
                        'master_id': cluster.master_id,
                        'slave_host': host,
                    }

                    response = self.server_response(host, server_message)
                    cluster.add_server(host, response['data']['id'])

                    cluster.new_servers[host] = {
                        'connected': True,
                        'initial_connection': True
                    }
                except Exception as e:
                    self.logger.warning(str(e))
                    self.logger.warning(traceback.format_exc())
                    cluster.new_servers[host] = {
                        'connected': False,
                        'initial_connection': True,
                        'error': str(e)
                    }

    def server_response(self, hostname, data):
        conn_list = hostname.split(':', 1)
        if len(conn_list) != 2:
            raise Exception('Invalid host name: {}'.format(hostname))
        with Client(conn_list[0], int(conn_list[1])) as c:
            response = BSON.decode(c.send(BSON.encode(data)))
            if response.get('error'):
                raise ClusterProtocolError('Error: {}'.format(response.get('error')))
            if response.get('data', {}).get('id') != data['id']:
                raise ClusterProtocolError()
            return response
