from .cluster import cluster
from .errors import MasterCommandError, SlaveCommandError, ClusterProtocolError


def master_only(f):
    def wrapper_f(*args):
        # 'args[0]' is the 'self' parameter of a method
        if cluster.is_cluster and not cluster.this_is_master:
            raise MasterCommandError(cluster.master_host)
        return f(*args)

    return wrapper_f


def slave_only(f):
    def wrapper_f(*args):
        # 'args[0]' is the 'self' parameter of a method
        if cluster.this_is_master:
            raise SlaveCommandError('Server {} is master'.format(cluster.master_host))
        return f(*args)

    return wrapper_f


def verified_only(f):
    def wrapper_f(*args):
        try:
            if not args[0]._is_verified:
                raise ClusterProtocolError('Connection was not verified')
        except ClusterProtocolError:
            raise
        except Exception as e:
            raise ClusterProtocolError('Message could not be decoded: {}'.format(str(e)))
        return f(*args)

    return wrapper_f

