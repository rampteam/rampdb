import asyncio
import logging
import struct
import traceback

from bson import BSON
from time import sleep

from .cluster import cluster
from ._decorators import verified_only
from .errors import SlaveCommandError, ClusterProtocolError
from ..config import settings
from ..socketserver import Client


class ReplicationClientProtocol(asyncio.Protocol):
    _message = b''
    __logger = None
    is_verified = False
    _should_run = True
    _handler = None

    def __init__(self, loop):
        self.message = {
            'command': 'slave_connection',
            'id': cluster.my_id
        }
        self.loop = loop

    def connection_made(self, transport):
        self.transport = transport
        self.write(self.message)

    def data_received(self, data):
        self._message += data
        message = None
        if len(self._message) > 4:
            try:
                bytes_to_receive = struct.unpack('i', self._message[0:4])[0]
                if len(self._message[4:]) != bytes_to_receive:
                    return
                # noinspection PyTypeChecker
                message = BSON.decode(self._message[4:])
                self._message = b''
                self.handle_response(message)
            except Exception as e:
                self.logger.warning(str(e))
                # self.logger.warning('DATA: {}'.format(message))
                self.shutdown()

    def handle_response(self, message):
        if not message.get('ok'):
            self.logger.warning('Command failed: {}'.format(message))
            self.shutdown()
            return

        data = message.get('data', {})
        if data['r_to'][0] == '_':
            raise SlaveCommandError('Command response unknown: {}'.format(data['r_to']))

        handler = self.handler
        db_cmd = getattr(handler, data['r_to'], None)
        if not callable(db_cmd):
            raise SlaveCommandError('Command {} is invalid'.format(data['r_to']))

        db_cmd(data)
        command = {
            'command': 'replication',
            'last_key': cluster.last_commit_key,
            'batch_size': self.batch_size,
        }
        self.write(command)

    @property
    def handler(self):
        if not self._handler:
            self._handler = _Handler(self)
        return self._handler

    def write(self, data: dict):
        message = BSON.encode(data)
        msg_len = struct.pack('I', len(message))
        message = msg_len + message
        self.transport.write(message)

    def connection_lost(self, exc):
        self.logger.warning('The server closed the connection')
        self.logger.warning('Stop the event lop')
        self.loop.stop()

    def shutdown(self):
        self.transport.close()

    def terminate(self):
        self._should_run = False

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger

    @property
    def batch_size(self):
        return int(settings.get('max_batch_size', section='replication', default=100))


class _Handler:
    _client = None

    def __init__(self, client):
        self._client = client

    @property
    def _is_verified(self):
        return self._client.is_verified

    def slave_connection(self, data: dict):
        if not self._client.is_verified and data.get('id') != cluster.my_id:
            raise SlaveCommandError('Handshake failed with master'
                                    ' message received: {}'.format(data))
        self._client.is_verified = True

    @verified_only
    def replication(self, data):
        ops = data.get('ops')
        if ops:
            print('===========================')
            print('REPLICATION')
            print(len(ops))
            print('===========================')
            with Client('127.0.0.1', int(settings.get('client_port', section='server'))) as c:
                response = c.send(BSON.encode({
                    'c': 's.replicate',
                    'p': {
                        'id': cluster.replica_id,
                        'raw_data': ops,
                    }
                }))
                message = BSON.decode(response)
                if not message.get('ok'):
                    sleep(5)
                    raise ClusterProtocolError('Replication failed: {}'.format(response))
                last_op_key = data.get('ops').pop(-1)[0]
                cluster.last_commit_key = last_op_key
        else:
            sleep(1)
