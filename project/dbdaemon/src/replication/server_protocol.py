import asyncio
import logging
import struct
import traceback

from bson import BSON
from time import monotonic
from .cluster import cluster
from ._decorators import master_only, slave_only, verified_only
from .errors import ClusterProtocolError
from ..config import settings
from ..errors import InvalidCommandError


class ReplicaCommunicationProtocol(asyncio.Protocol):
    __logger = None
    _message = b''
    _handler = None
    _loop = None
    commit_log = None
    connected_verified = False
    slave_id = None

    def __init__(self, *args, **kwargs):
        super(*args, **kwargs)
        self._loop = asyncio.get_event_loop()

    def connection_made(self, transport):
        self.transport = transport

    def connection_lost(self, exc):
        self.logger.info('CONNECTION LOST')

    def data_received(self, data):
        self._message += data
        if len(self._message) > 4:
            try:
                bytes_to_receive = struct.unpack('i', self._message[0:4])[0]
                if len(self._message[4:]) != bytes_to_receive:
                    return
                # noinspection PyTypeChecker
                message = BSON.decode(self._message[4:])
                self._message = b''
                asyncio.async(self.handle_response(message))
            except Exception as e:
                self.logger.warning(str(e))
                asyncio.async(self.write({'ok': 0, 'error': str(e)}))
                self.transport.close()

    @asyncio.coroutine
    def handle_response(self, message):
        try:
            if message['command'][0] == '_':
                raise InvalidCommandError('Command unknown: {}'.format(message['command']))

            handler = self.handler
            db_cmd = getattr(handler, message['command'], None)
            if not callable(db_cmd):
                raise InvalidCommandError('Command {} is invalid'.format(message['command']))
            response = yield from db_cmd(message)
            asyncio.async(self.write({'ok': 1, 'data': response}))
        except Exception as e:
            self.logger.warning(traceback.format_exc())
            self.logger.warning(str(e))
            asyncio.async(self.write({'ok': 0, 'error': str(e)}, True))

    @asyncio.coroutine
    def write(self, data: dict, with_exit=False):
        message = BSON.encode(data)

        msg_len = struct.pack('I', len(message))
        message = msg_len + message
        self.transport.write(message)
        if with_exit:
            self.transport.close()

    def eof_received(self):
        self.logger.info('Close the client socket')
        self.transport.close()

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger

    @property
    def handler(self):
        if not self._handler:
            self._handler = ClusterCommands(self, self.commit_log)
        return self._handler


class ClusterCommands:
    _protocol = None
    __logger = None

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger

    def __init__(self, protocol, commit_log):
        self._protocol = protocol
        self.commit_log = commit_log

    @property
    def _is_verified(self):
        return self._protocol.connected_verified

    @asyncio.coroutine
    @slave_only
    def add_to_cluster(self, data):
        if data.get('name') != cluster.name:
            raise ClusterProtocolError('Cluster name is invalid: {}'.format(data.get('name')))
        cluster.master_id = data.get('master_id')
        cluster.master_host = data.get('master_host')
        cluster.my_host = data.get('slave_host')
        cluster.my_id = data.get('id')
        cluster.add_server(cluster.my_host, cluster.my_id)
        cluster.add_server(cluster.master_host, cluster.master_id)
        return {'id': data.get('id')}

    @asyncio.coroutine
    def cluster_status(self, data: dict):
        if data.get('id') != cluster.my_id:
            raise ClusterProtocolError('Id is not valid: {}'.format(data.get('id')))
        return {'r_to': 'cluster_status', 'status': {
            'servers': dict(cluster.servers),
            'master_host': cluster.master_host
        }}

    @asyncio.coroutine
    @master_only
    def slave_connection(self, data: dict):
        slave_id = data.get('id')
        if slave_id not in cluster.servers:
            raise ClusterProtocolError('Slave {} not registered'.format(slave_id))
        self._protocol.connected_verified = True
        self._protocol.slave_id = slave_id
        cluster.update(slave_id, 'connected', True)
        cluster.update(slave_id, 'last_connected', monotonic())
        return {'r_to': 'slave_connection', 'id': slave_id}

    @asyncio.coroutine
    @master_only
    @verified_only
    def replication(self, data: dict):
        cluster.update(self._protocol.slave_id, 'last_connected', monotonic())
        last_key = data.get('last_key', None)
        batch_size = data.get('batch_size', 1)
        return {'r_to': 'replication', 'ops': self.commit_log.read(last_key, batch_size)}

    @asyncio.coroutine
    def ping(self, data):
        if cluster.no_master:
            if data.get('master_id'):
                master_id, master_host = data.get('master_id'), data.get('master_host')
                if master_id and master_host and cluster.old_master_host != master_host:
                    self.logger.debug('=======================')
                    self.logger.debug('=======================')
                    self.logger.debug('ADDING MASTER {}'.format(master_host))
                    self.logger.debug('=======================')
                    self.logger.debug('=======================')
                    cluster.set_master(master_id, master_host)
        cluster.update(data.get('id'), 'last_connected', monotonic())
        cluster.update(data.get('id'), 'connected_servers', data.get('connected_servers'))
        cluster.update(data.get('id'), 'priority', data.get('priority'))
        cluster.update(data.get('id'), 'last_key', data.get('last_key'))
        cluster.update(data.get('id'), 'connected', True)
        ret = {
            'r_to': 'ping',
            'id': cluster.my_id,
            'master_id': cluster.master_id,
            'nr_servers': len(cluster.servers),
        }

        if data.get('nr_servers') != ret['nr_servers']:
            ret['servers'] = dict(cluster.servers)
        return ret

    @asyncio.coroutine
    def election(self, data):
        self.logger.debug('=======================')
        self.logger.debug('ELECTION TRIGGERED BY {}'.format(data.get('id')))
        self.ping(data)
        ret = {
            'r_to': 'election',
            'id': cluster.my_id,
            'election_ts': data.get('election_ts'),
            'vote': False,
        }
        self.logger.debug(
            'Can be elected: {}'.format(cluster.server_can_be_elected(data.get('id'))))
        self.logger.debug('Can vote: {}'.format(cluster.can_vote()))
        if cluster.can_vote() and cluster.server_can_be_elected(data.get('id')):
            self.logger.debug('VOTED YES')
            cluster.voted()
            ret['vote'] = True
        else:
            self.logger.debug('VOTED NO')
        self.logger.debug('=======================')
        return ret
