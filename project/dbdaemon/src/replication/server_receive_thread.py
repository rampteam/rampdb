import asyncio
import selectors
from setproctitle import setproctitle

from .server_protocol import ReplicaCommunicationProtocol
from src.stop_thread import StopThread
from ..config import settings


class ServerReceiveThread(StopThread):
    loop = None
    server = None

    def run(self):
        setproctitle('DbDaemon Receive thread')
        self.logger.debug('Starting replication server')
        asyncio.set_event_loop(asyncio.SelectorEventLoop(selector=selectors.EpollSelector()))

        self.loop = asyncio.get_event_loop()
        self.loop.set_debug(True)
        coro = self.loop.create_server(
            ReplicaCommunicationProtocol,
            self.bind_host,
            self.bind_port
        )
        self.server = self.loop.run_until_complete(coro)
        self.loop.call_later(1, self.stop)
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        self.server.close()
        self.loop.run_until_complete(self.server.wait_closed())
        self.loop.close()

    def stop(self):
        if not self._should_run:
            self.loop.stop()
            return
        self.loop.call_later(1, self.stop)

    def terminate(self):
        super().terminate()
        self.logger.debug('Terminating replication server')

    @property
    def bind_host(self):
        return settings.get('server_host', section='replication')

    @property
    def bind_port(self):
        return int(settings.get('server_port', section='replication'))
