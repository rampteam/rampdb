import asyncio
import logging
import selectors
import struct
import traceback

from bson import BSON
from math import floor
from setproctitle import setproctitle
from time import sleep, monotonic

from src.stop_thread import StopThread
from .cluster import cluster
from .errors import ClusterProtocolError, ServerAlreadyAdded
from ..config import settings
from queue import Queue, Empty


class GossipThread(StopThread):
    loop = None
    server = None
    _connections = None
    client_stopped = False

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._connections = {}
        self.queue = Queue()

    def is_majority_down(self, down_servers):
        if not down_servers:
            return False
        if down_servers > cluster.failure_tolerance:
            return True
        return False

    @property
    def majority(self):
        return floor(len(cluster.servers) / 2) + 1

    def run(self):
        setproctitle('Dbdaemon Gossip thread')
        self.logger.debug('Starting replication server')
        asyncio.set_event_loop(asyncio.SelectorEventLoop(selector=selectors.EpollSelector()))

        self.loop = asyncio.get_event_loop()

        def handler(loop, context):
            print('EXCEPTION HANDLER')
            print(loop)
            print(context)

        self.loop.set_exception_handler(handler)
        self.loop.set_debug(True)
        self.loop.call_later(1, self.stop)
        self.loop.call_later(1, self.check_down_servers)
        self.loop.call_soon(self.add)
        self.loop.call_soon(self.election)
        self.loop.run_forever()

        self.loop.close()

    def election(self):
        try:
            if cluster.can_trigger_election:
                self.logger.debug('THIS SERVER CAN TRIGGER ELECTIONS')
                cluster.last_election_triggered_by_me = monotonic()
                cluster.send_election_signal = True
                cluster.votes_received = 0
                self.loop.call_later(0.5, self.election_response)
            self.loop.call_later(2, self.election)
        except Exception as e:
            self.logger.warning('Election exception')
            self.logger.warning(str(e))
            self.logger.warning(traceback.format_exc())

    def election_response(self):
        self.logger.debug('==========================')
        self.logger.debug('VALID ELECTION: {}'.format(cluster.valid_election))
        self.logger.debug('MAJORITY: {}'.format(cluster.majority))
        self.logger.debug('VOTES RECEIVED: {}'.format(cluster.votes_received))
        self.logger.debug('==========================')
        if cluster.valid_election:
            if cluster.votes_received >= cluster.majority:
                cluster.master = 'me'
                cluster.set_master(cluster.my_id, cluster.my_host)
                self.logger.debug('==========================')
                self.logger.debug('==========================')
                self.logger.debug('SERVER PROMOTED TO MASTER')
                self.logger.debug('==========================')
                self.logger.debug('==========================')
                return
            self.loop.call_later(0.5, self.election_response)

    def check_down_servers(self):
        try:
            down_servers = cluster.down_servers
            if cluster.this_is_master and self.is_majority_down(down_servers):
                self.logger.warning('STEP DOWN AS MASTER')
                cluster.remove_master()
            elif cluster.master_is_down:
                cluster.remove_master()
                self.logger.warning('MASTER IS DOWN, removing it')
            self.loop.call_later(2, self.check_down_servers)
        except Exception as e:
            self.logger.warning('Down server exception')
            self.logger.warning(str(e))
            self.logger.warning(traceback.format_exc())

    def add(self):
        if not self._should_run:
            return
        if cluster.servers_changed:
            servers = dict(cluster.servers)
            for id, server in servers.items():
                if id in self._connections:
                    continue
                self._start_client(id, server['host'])
            cluster.servers_changed = False

        if self.client_stopped:
            while not self.queue.empty():
                try:
                    id = self.queue.get(True, 1)
                    self._connections.pop(id, None)
                    host = cluster.servers[id]['host']
                    self._start_client(id, host)
                except Empty:
                    break
                except AttributeError:
                    pass
        self.loop.call_later(2, self.add)

    def _start_client(self, id, hostname):
        try:
            host, port = hostname.split(':', 1)
            self.logger.debug('TRY TO CONNECT TO: {}'.format(hostname))
            port = int(port)
            corutine = self.loop.create_connection(
                lambda: GossipClientProtocol(self.loop, self, id),
                host, port)
            task = asyncio.async(corutine)

            def call(task, *args, **kwargs):
                if task.exception():
                    self.queue.put(id)

            task.add_done_callback(call)
            self._connections[id] = task
        except ConnectionRefusedError:
            pass

    def stop(self):
        if not self._should_run:
            self.loop.stop()
            return
        sleep(1)
        self.loop.call_later(1, self.stop)

    def terminate(self):
        super().terminate()
        self.logger.debug('Terminating replication server')


class GossipClientProtocol(asyncio.Protocol):
    _message = b''
    __logger = None
    is_verified = False
    _should_run = True
    _handler = None
    _thread = None
    _my_id = None
    _last_election_sent = 0

    def __init__(self, loop, thread, id):
        self._my_id = id
        self._thread = thread
        self.message = {
            'command': 'ping',
            'id': cluster.my_id
        }
        self.loop = loop

    def connection_made(self, transport):
        self.transport = transport
        self.logger.debug('CONNECTION MADE')
        self.write(self.message)

    def data_received(self, data):
        if not self._should_run:
            self.shutdown()
        self._message += data
        message = None
        if len(self._message) > 4:
            try:
                bytes_to_receive = struct.unpack('i', self._message[0:4])[0]
                if len(self._message[4:]) != bytes_to_receive:
                    return
                # noinspection PyTypeChecker
                message = BSON.decode(self._message[4:])
                self._message = b''
                self.handle_response(message)
            except Exception as e:
                self.logger.warning(str(e))
                self.logger.warning('DATA: {}'.format(message))
                self.logger.warning(traceback.format_exc())
                self.shutdown()

    def handle_response(self, message):
        if not message.get('ok'):
            self.logger.warning('Command failed: {}'.format(message))
            self.shutdown()
            return

        data = message.get('data', {})
        if data['r_to'][0] == '_':
            raise ClusterProtocolError('Command response unknown: {}'.format(data['r_to']))

        handler = self.handler
        db_cmd = getattr(handler, data['r_to'], None)
        if not callable(db_cmd):
            raise ClusterProtocolError('Command {} is invalid'.format(data['r_to']))
        db_cmd(data)

        if not cluster.no_master:
            self.send_ping()
        else:
            self.check_master_down()
            # master is down
            # must be up to date with the last write operations

    def send_ping(self):
        self._send_command('ping')

    def _send_command(self, command, additional_data=None):
        nr_servers = len(cluster.servers)
        command = {
            'command': command,
            'nr_servers': nr_servers,
            'connected_servers': nr_servers - cluster.down_servers,
            'master_id': cluster.master_id,
            'master_host': cluster.master_host,
            'id': cluster.my_id,
            'last_key': cluster.last_commit_key,
            'priority': cluster.priority,
        }
        if additional_data:
            command.update(additional_data)
        self.loop.call_later(3, self.write, command)

    def check_master_down(self):
        if cluster.valid_election \
                and self._last_election_sent != cluster.last_election_triggered_by_me:
            self.logger.debug('TRIGGER ELECTION')
            self._last_election_sent = cluster.last_election_triggered_by_me
            self._send_command('election', {'election_ts': self._last_election_sent})
            return
        self.send_ping()

    @property
    def handler(self):
        if not self._handler:
            self._handler = _Handler(self)
        return self._handler

    def write(self, data: dict):
        if cluster.my_host.split(':')[0] != self.transport.get_extra_info('peername')[0]:
            self.logger.debug(
                'SEND PING TO {}'.format(self.transport.get_extra_info('peername')[0]))
        message = BSON.encode(data)
        msg_len = struct.pack('I', len(message))
        message = msg_len + message
        self.transport.write(message)

    def connection_lost(self, exc):
        self._thread.client_stopped = True
        self._thread.queue.put(self._my_id)
        self.logger.warning('The server closed the connection')

    def shutdown(self):
        self.transport.close()

    def terminate(self):
        self._should_run = False

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger

    @property
    def batch_size(self):
        return int(settings.get('max_batch_size', section='replication', default=100))


class _Handler:
    _client = None

    def __init__(self, client):
        self._client = client

    def ping(self, data):
        if data.get('nr_servers') != len(cluster.servers):
            for id, server in data.get('servers', {}).items():
                if id not in cluster.servers:
                    try:
                        host = server['host']
                        cluster.add_server(host, id, cluster.master_host == host)
                    except ServerAlreadyAdded:
                        pass
        return 'done'

    def election(self, data):
        if data.get('election_ts') == cluster.last_election_triggered_by_me:
            if data.get('vote', False):
                with cluster.vote_lock:
                    cluster.votes_received = cluster.votes_received + 1
        return 'done'
