import json
import logging

from bson import BSON
from os import urandom
from math import floor
from multiprocessing import Manager, Lock
from time import monotonic
from uuid import uuid4
from .errors import ServerAlreadyAdded, ClusterAlreadyActive, ClusterNotInitialized
from ..config import settings
from ..socketserver import Client


class Cluster:
    servers = None
    _manager = None
    __replica_id = None
    _config_file = None
    _down_timeout = 10
    node_timeout = 5
    vote_lock = None
    __logger = None

    def __init__(self):
        self.__replica_id = urandom(128)

    def reset(self):
        self.vote_lock = Lock()
        if self._manager:
            self._manager.shutdown()
        self._manager = Manager()
        self.servers = self._manager.dict()
        self._new_servers = self._manager.dict()
        self._meta = self._manager.dict()
        self.priority = int(settings.get('priority', section='replication'))

    def init(self):
        self._down_timeout = float(settings.get('down_timeout', section='replication'))
        self.reset()
        try:
            f = open(settings.get('config_path', section='replication'))
            cluster_status = json.load(f)
            self._config_file = open(settings.get('config_path', section='replication'), mode='w')

            self.is_cluster = cluster_status['meta']['is_cluster']
            self.my_id = cluster_status['meta']['my_id']
            self.my_host = cluster_status['meta']['my_host']

            del cluster_status['servers'][self.my_id]
            connection_success = True
            for id, server in cluster_status['servers'].items():
                try:
                    data = {
                        'command': 'cluster_status',
                        'id': id
                    }
                    response = self._server_status(server['host'], data)
                    response = response['data']['status']
                    self.master_host = master_host = response['master_host']

                    for id, server in response['servers'].items():
                        self.add_server(server['host'], id, server['host'] == master_host)
                        if server['host'] == master_host:
                            self.master_id = id
                    if self.my_host == master_host:
                        self.master = 'me'
                    connection_success = True
                    break
                except Exception as e:
                    print('ERROR: {}'.format(str(e)))
            if not connection_success:
                raise Exception()
        except Exception:
            self.reset()
            self._config_file = open(settings.get('config_path', section='replication'), mode='w')

    def _server_status(self, host: str, data: dict):
        conn_list = host.split(':', 1)
        with Client(conn_list[0], int(conn_list[1])) as c:
            response = BSON.decode(c.send(BSON.encode(data)))
            if response.get('error'):
                raise Exception('Error: {}'.format(response.get('error')))
            return response

    def update(self, server_key, key, value):
        server = self.servers[server_key]
        server[key] = value
        self.servers[server_key] = server
        self.write_config()

    def add_server(self, host, id, is_master=False):
        if id in self.servers:
            raise ServerAlreadyAdded('Server with id {} already exists'.format(id))
        self.servers[id] = dict(host=host, connected=is_master, last_connected=monotonic())
        self.write_config()
        self.servers_changed = True
        self.nr_servers += 1

    def add_new_server(self, hostname):
        if not self.is_active or not self.this_is_master:
            raise ClusterNotInitialized()
        for k, v in self.servers.items():
            if v['host'] == hostname:
                raise ServerAlreadyAdded()
        self._new_servers[hostname] = {
            'initial_connection': False,
            'connected': False,
            'error': None,
        }

    def write_config(self):
        meta_keys = ['is_cluster', 'my_id', 'my_host']
        meta = {k: self._meta[k] for k in meta_keys}
        data = {
            'servers': dict(self.servers),
            'meta': meta,
        }
        self._config_file.truncate(0)
        self._config_file.seek(0)
        json.dump(data, self._config_file)
        self._config_file.flush()

    @property
    def replica_id(self):
        return self.__replica_id

    @property
    def last_commit_key(self):
        return self._meta.get('last_commit_key')

    @last_commit_key.setter
    def last_commit_key(self, value):
        self._meta['last_commit_key'] = value

    @property
    def my_host(self):
        return self._meta.get('my_host')

    @my_host.setter
    def my_host(self, value):
        self._meta['my_host'] = value

    @property
    def my_id(self):
        return self._meta.get('my_id')

    @my_id.setter
    def my_id(self, value):
        self._meta['my_id'] = value

    @property
    def master_id(self):
        return self._meta.get('_master_id')

    @master_id.setter
    def master_id(self, value):
        self._meta['_master_id'] = value

    @property
    def master(self):
        return self._meta.get('master')

    @master.setter
    def master(self, val):
        self._meta['master'] = val

    @property
    def master_host(self):
        return self._meta.get('master_host')

    @master_host.setter
    def master_host(self, val):
        self._meta['master_host'] = val

    @property
    def name(self):
        return self._meta.get('_cluster_name')

    @property
    def servers_changed(self):
        return self._meta.get('servers_changed', False)

    @servers_changed.setter
    def servers_changed(self, value):
        self._meta['servers_changed'] = value

    @name.setter
    def name(self, val):
        self._meta['_cluster_name'] = val

    @property
    def nr_servers(self):
        return self._meta.get('nr_servers', 0)

    @nr_servers.setter
    def nr_servers(self, value):
        self._meta['nr_servers'] = value

    @property
    def is_cluster(self):
        return self._meta.get('is_cluster')

    @is_cluster.setter
    def is_cluster(self, val):
        self._meta['is_cluster'] = val

    @property
    def connected_servers(self):
        return len([s for k, s in self.servers.copy().items() if s.get('connected') is True])

    @property
    def status(self):
        servers = self.servers.copy()
        if not self.is_active:
            if self.is_cluster:
                return 'Cluster "{}" is not configured'.format(self.name)
            return 'Cluster is not activated'
        status = {
            'servers': {},
            'number_of_servers': self.nr_servers,
            'connected_servers': self.connected_servers,
            'cluster_name': self.name,
        }
        for id, data in servers.items():
            status['servers'][data['host']] = {
                'connected': data.get('connected'),
                'is_master': self.master_host == data['host'],
            }
        return status

    def new_servers_exist(self):
        return len(self.new_servers) != 0

    def set_this_as_master(self, my_host: str):
        if not self.is_cluster:
            raise ClusterNotInitialized('Cluster was not initialized, please start instance '
                                        'as replication server')
        if self.is_active:
            raise ClusterAlreadyActive('Another cluster is active: {}'.format(self.name))
        self.is_cluster = True
        self.master = 'me'
        self.master_host = my_host
        self.my_host = my_host
        self.my_id = self.master_id = uuid4().hex
        self.add_server(my_host, self.my_id, is_master=True)

    @property
    def old_master_host(self):
        return self._meta.get('old_master_host', None)

    @old_master_host.setter
    def old_master_host(self, value):
        self._meta['old_master_host'] = value

    def remove_master(self):
        self.old_master_host = self.master_host
        self.master = None
        self.master_host = None
        self.master_id = None

    def set_master(self, id, host):
        self.master_host = host
        self.master_id = id

    def put_on_standby(self, cluster_name):
        self.is_cluster = True
        self.name = cluster_name

    @property
    def priority(self):
        return self._meta['priority']

    @priority.setter
    def priority(self, value):
        self._meta['priority'] = int(value)

    @property
    def last_election_triggered_by_me(self):
        return self._meta.get('last_election', 0)

    @last_election_triggered_by_me.setter
    def last_election_triggered_by_me(self, value):
        self._meta['last_election'] = value

    @property
    def votes_received(self):
        return self._meta.get('votes_received', 0)

    @votes_received.setter
    def votes_received(self, value):
        self._meta['votes_received'] = value

    @property
    def send_election_signal(self):
        return self._meta.get('send_election_signal', 0)

    @send_election_signal.setter
    def send_election_signal(self, value):
        self._meta['send_election_signal'] = value

    def voted(self):
        self._meta['voted'] = monotonic()

    def can_vote(self):
        return monotonic() > self._meta.get('voted', 0) + self.node_timeout * 2

    @property
    def valid_election(self):
        return monotonic() <= self.last_election_triggered_by_me + self.node_timeout * 2

    @property
    def can_trigger_election(self):
        # has been an election in the last node_timeout
        time = monotonic()
        if time <= self._meta.get('voted', 0) + self.node_timeout * 2:
            return False
        t = time > self.last_election_triggered_by_me + self.node_timeout * 4
        return t and self.can_be_elected

    @property
    def can_be_elected(self):
        # can reach the most servers in the cluster
        if self.down_servers > self.failure_tolerance and self.nr_servers > 0:
            self.logger.debug('====================')
            self.logger.debug('MAJORITY CAN NOT BE CONTACTED')
            self.logger.debug('TOTAL SERVERS: {}'.format(self.nr_servers))
            self.logger.debug('DOWN SERVERS: {}'.format(self.down_servers))
            self.logger.debug('====================')
            return False
        return self.server_can_be_elected(self.my_id)

    def server_can_be_elected(self, server_id):
        if not self.no_master:
            return False
        s = self.servers.get(server_id)
        last_key = s.get('last_key')
        last_ts = 0
        if last_key:
            last_ts = float(last_key.split('||', 1)[0])

        servers = self.servers
        for id, server in servers.items():
            if id == server_id:
                continue
            # must be up to date with the last write operations
            last_srv_key = server.get('last_key')
            if last_srv_key:
                ts = float(last_srv_key.split('||', 1)[0])
                if ts > last_ts:
                    return False
                # has the highest priority from the available servers
                # but the remote server has to be at least as up to date as this server
                if not self.is_down(server) and server.get('priority', 1) > s.get('priority',
                                                                                  1) and last_ts <= ts:
                    return False
        return True

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger

    @property
    def majority(self):
        return floor(self.nr_servers / 2) + 1

    @property
    def failure_tolerance(self):
        return self.nr_servers - self.majority

    @property
    def this_is_master(self):
        return self.master == 'me'

    @property
    def no_master(self):
        return self.master_host is None and self.nr_servers != 0

    @property
    def master_is_down(self):
        if not self.master_id:
            return False
        return monotonic() - self._down_timeout > self.servers[self.master_id]['last_connected']

    def is_down(self, server):
        return monotonic() - self._down_timeout > server['last_connected']

    @property
    def down_servers(self):
        time = monotonic()
        down_timeout = self._down_timeout
        servers_down = 0
        for id, server in self.servers.items():
            if time - down_timeout > server['last_connected']:
                self.update(id, 'connected', False)
                servers_down += 1
        return servers_down

    @property
    def is_active(self):
        if self.nr_servers:
            return True
        return self.is_cluster and (self.master_id is not None or self.this_is_master)

    @property
    def new_servers(self) -> dict:
        return self._new_servers


cluster = Cluster()
