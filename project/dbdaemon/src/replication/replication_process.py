import asyncio
import logging
import selectors
import traceback

from multiprocessing import Process
from setproctitle import setproctitle
from signal import (
    SIGTERM,
    SIGHUP,
    SIGINT,
    SIGQUIT,
    signal
)
from time import sleep

from .client_protocol import ReplicationClientProtocol
from .cluster import cluster
from .gossip_thread import GossipThread
from .member_add_thread import MemberAddThread
from .server_protocol import ReplicaCommunicationProtocol as ServerCommunicationProtocol
from .server_receive_thread import ServerReceiveThread
from ..commit_log import ReadCommitLogThread
from ..config import settings


class ReplicationProcess(Process):
    setproctitle('DbDaemon Replication Process')
    server_thread = None
    member_add_thread = None
    gossip_thread = None
    loop = None
    _commit_log = None
    _should_work = True
    __logger = None

    _master_socket = None

    def run(self):
        setproctitle('Replication process')
        self._redirect_signals()
        self.logger.debug('Start Replication Process')
        self.server_thread = ServerReceiveThread()
        self.member_add_thread = MemberAddThread()
        self._commit_log = ReadCommitLogThread()
        self.gossip_thread = GossipThread()

        cluster.last_commit_key = self._commit_log.last_disk_key
        ServerCommunicationProtocol.commit_log = self._commit_log

        self.server_thread.start()
        self.member_add_thread.start()
        self._commit_log.start()
        self.gossip_thread.start()
        self.run_client()

        self.member_add_thread.join()
        self.server_thread.join()
        self._commit_log.join()
        self.gossip_thread.join()

    def run_client(self):
        asyncio.set_event_loop(asyncio.SelectorEventLoop(selector=selectors.EpollSelector()))
        self.loop = asyncio.get_event_loop()
        self.loop.set_debug(True)

        while self._should_work:
            try:
                if not cluster.this_is_master and cluster.master_id:
                    self.connect_to_master()
                else:
                    sleep(1)
                    continue

            except Exception as e:
                self.logger.warning(traceback.format_exc())
                self.logger.warning(str(e))
                self._master_socket = None
                sleep(10)
            if self._should_work:
                sleep(10)
        self.loop.close()

    def connect_to_master(self):
        if cluster.master_host:
            host, port = cluster.master_host.split(':', 1)
            coro = self.loop.create_connection(
                lambda: ReplicationClientProtocol(self.loop),
                host, int(port)
            )
            asyncio.async(coro)
            self.loop.run_forever()

    def _redirect_signals(self):
        signal(SIGTERM, self._stop_signal)
        signal(SIGHUP, self._stop_signal)
        signal(SIGINT, self._stop_signal)
        signal(SIGQUIT, self._stop_signal)

    def _stop_signal(self, signum, frame):
        self._should_work = False
        self.logger.info('Received signal {}'.format(signum))
        if self.loop:
            self.loop.stop()
        if self.server_thread:
            self.server_thread.terminate()
        if self.member_add_thread:
            self.member_add_thread.terminate()
        if self._commit_log:
            self._commit_log.terminate()
        if self.gossip_thread:
            self.gossip_thread.terminate()

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger
