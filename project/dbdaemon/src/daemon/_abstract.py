from signal import (
    SIGUSR1,
    SIGUSR2,
    SIGTERM,
    SIGHUP,
    SIGINT,
    SIGQUIT,
    SIGKILL,
    signal
)
import atexit
import logging
import os
import sys
import time
import traceback

from ..errors import InitError

LOG_FORMAT = "[%(asctime)s]|%(levelname)s: %(message)s"


class AbstractDaemon:
    _shutdown_sent = False
    stdin = '/dev/null'
    stdout = '/dev/null'
    stderr = '/dev/null'
    pid_file = '/tmp/daemon.pid'
    logger_name = 'daemon_logger'
    log_level = 'DEBUG'
    logger = None

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.set_logging()

    def run(self):
        raise NotImplementedError('Run method must be implemented')

    def terminate(self):
        raise NotImplementedError("Terminate must be implemented is not implemented")

    def signal_usr_1(self, signum, frame):
        pass

    def signal_usr_2(self, signum, frame):
        pass

    def start(self):
        if self._pid:
            print('Pid exists: {}'.format(self.pid_file))
            raise InitError()
        self._daemonize()
        self._redirect_signals()
        try:
            self.run()
        except Exception as e:
            self.logger.critical("Global Error caught: {} {}".format(e, traceback.format_exc()))

    def stop(self):
        if not self._pid:
            message = "PID file {} does not exist.".format(self.pid_file)
            print(message, file=sys.stderr)
            return
        else:
            self.logger.debug("PID read: {}".format(self._pid))

        # Try killing the daemon process
        try:
            pid = self._pid
            while 1:
                self.logger.info('Sending signal {}'.format(SIGTERM))
                os.kill(pid, SIGTERM)
                time.sleep(1)
        except OSError as err:
            if str(err).find("No such process") != 0:
                self._delete_pid_file()
            else:
                self.logger.critical(str(err))
                sys.exit(1)
        self.logger.debug("Daemon exit.")

    @property
    def _pid(self):
        try:
            with open(self.pid_file, mode='r') as pf:
                pid = pf.read().strip()
            return int(pid)
        except IOError:
            return None

    def set_logging(self):
        logger = logging.getLogger(self.logger_name)
        self.logger = logger
        logger.setLevel(self.log_level)

        handler = logging.StreamHandler(stream=sys.stderr)
        handler.setLevel(self.log_level)
        handler.setFormatter(logging.Formatter(LOG_FORMAT, '%H:%M:%S'))

        logger.addHandler(handler)

    def _daemonize(self):
        self.logger.debug("Daemon init.")
        try:
            pid = os.fork()
            if pid > 0:
                self.logger.debug("First parent shutdown.")
                sys.exit(0)
            else:
                self.logger.debug("Second process started.")
        except OSError as e:
            self.logger.critical("Fork 1 failed: {} {}".format(e.errno, e.strerror))
            sys.exit(1)

        os.chdir("/")
        os.setsid()
        os.umask(0)

        try:
            pid = os.fork()
            if pid > 0:
                self.logger.debug("Second parent shutdown.")
                os.kill(os.getpid(), SIGKILL)
            else:
                self.logger.debug("Third process started.")
        except OSError as e:
            self.logger.critical("Fork 2 failed: {} {}".format(e.errno, e.strerror))
            sys.exit(1)

        sys.stdout.flush()
        sys.stderr.flush()
        with open(self.stdin, mode='r') as si:
            os.dup2(si.fileno(), sys.stdin.fileno())

        with open(self.stdout, mode='a+') as so:
            os.dup2(so.fileno(), sys.stdout.fileno())

        with open(self.stderr, mode='a+', buffering=1) as se:
            os.dup2(se.fileno(), sys.stderr.fileno())

        try:
            with open(self.pid_file) as f:
                pid = int(f.readline().strip('\n'))
        except Exception:
            pass
        else:
            try:
                os.kill(pid, 0)
            except ProcessLookupError:
                pass
            else:
                raise EnvironmentError("A process with the previous PID is already running!")

        atexit.register(self._delete_pid_file)
        pid = str(os.getpid())
        with open(self.pid_file, mode='w+') as f:
            f.write("{}\n".format(pid))
        self.logger.info("Daemon created.")

    def _delete_pid_file(self):
        try:
            os.remove(self.pid_file)
        except OSError as e:
            self.logger.warning(str(e))

    def _stop_signal(self, signum, frame):
        if not self._shutdown_sent:
            self.logger.info('Received signal {}'.format(signum))
            self._shutdown_sent = True
            self.terminate()

    def _redirect_signals(self):
        signal(SIGTERM, self._stop_signal)
        signal(SIGHUP, self._stop_signal)
        signal(SIGINT, self._stop_signal)
        signal(SIGQUIT, self._stop_signal)

        signal(SIGUSR1, self.signal_usr_1)
        signal(SIGUSR2, self.signal_usr_2)
