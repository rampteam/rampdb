import asyncio
import selectors

from setproctitle import setproctitle

from ._abstract import AbstractDaemon
from ..commit_log import commit_log
from ..config import settings
from ..errors import ConfigError
from ..socketserver import ServerReceiveProtocol
from ..replication import ReplicationProcess, cluster
from ..pool import ObjectPool
from ..wire import ServerProtocol
import src.socketserver.protocol as protocol

protocol.pool = ObjectPool(ServerProtocol, max_size=100)


class Daemon(AbstractDaemon):
    _should_run = True
    _replication = None

    def __init__(self):
        log_file = settings.get('error_log')
        super().__init__(
            log_level=settings.get('log_level', 'INFO').upper(),
            stdout=log_file,
            stderr=log_file,
            pid_file=settings.get('pid_file', None),
            logger_name=settings.get('logger_name'),
        )

    def run(self):
        cluster.init()
        host = settings.get('client_host', '127.0.0.1', section='server')
        port = settings.get('client_port', section='server')
        if not port:
            raise ConfigError('Client port not found')
        port = int(port)
        commit_log.cluster = cluster
        commit_log.start()

        self.start_server(host, port)

        # replication manager
        self.start_replication()
        setproctitle('DbDaemon Main Process')

        # run
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            pass
        self.server.close()
        self.loop.run_until_complete(self.server.wait_closed())
        self.loop.close()

        commit_log.join()
        if self._replication:
            self._replication.join()
        self.logger.info('Daemon stop')

    def start_server(self, host, port):
        self.logger.debug('Starting replication server')
        asyncio.set_event_loop(asyncio.SelectorEventLoop(selector=selectors.EpollSelector()))

        self.loop = asyncio.get_event_loop()
        self.loop.set_debug(True)
        coro = self.loop.create_server(ServerReceiveProtocol, host, port)
        self.server = self.loop.run_until_complete(coro)
        self.loop.call_later(1, self.stop_callback)

    def stop_callback(self):
        if not self._should_run:
            self.loop.stop()
            return
        self.loop.call_later(1, self.stop_callback)

    def start_replication(self):
        if settings.get('active', section='replication') == 'True':
            cluster_name = settings.get('cluster_name', section='replication')
            if cluster_name:
                cluster.put_on_standby(cluster_name)
            self._replication = ReplicationProcess(name='DbDaemon Replication Process')
            self._replication.start()

    def terminate(self):
        self.logger.debug('Terminate called')
        self._should_run = False
        commit_log.terminate()
        if self._replication:
            self.logger.debug('Termination replication process')
            self._replication.terminate()
        self.logger.debug('Terminate exit')
