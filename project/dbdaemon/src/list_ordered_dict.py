from collections import OrderedDict


class ListOrderedDict(OrderedDict):
    def first_key(self):
        for key in self:
            return key
        raise ValueError("OrderedDict() is empty")

    def next_key(self, prev_key):
        try:
            return self._OrderedDict__map[prev_key].next.key
        except AttributeError:
            raise ValueError("{!r} is the last key".format(prev_key))
