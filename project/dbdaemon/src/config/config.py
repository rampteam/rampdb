import os
import json
from configparser import ConfigParser

from .defaults import default_values
from ..errors import ConfigError


class Settings(object):
    _default_section = 'general'
    _config_file = None
    config = None

    def __init__(self):
        self.config = {}
        self.set_defaults()

    def __str__(self):
        return str(json.dumps(self.config, indent=2, sort_keys=True))

    def set_defaults(self):
        self.update(default_values.copy())

    def set_config(self, path: str):
        if not os.path.exists(path):
            raise ConfigError('Config file not found {}'.format(path))
        self._config_file = path
        self.update(self.to_dict(path))

    def reset(self):
        self.config = {}
        self.set_defaults()
        self.set_config(self._config_file)

    def update(self, options: dict):
        for k, v in options.items():
            self.config.setdefault(k, {}).update(v)

    def get(self, key: str, default=None, section:str=None):
        section = section or self._default_section
        return self.config.get(section, {}).get(key, None) or default

    @staticmethod
    def to_dict(conf_file_path):
        c = ConfigParser()
        c.read(conf_file_path)
        dict_conf = {}
        for s in c.sections():
            dict_conf[s] = dict(c.items(s))
        return dict_conf

    def __repr__(self):
        return self.config

settings = Settings()
