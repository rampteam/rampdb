default_values = {
    'general': {
        'pid_file': '/tmp/dbdaemon.pid',
        'log_level': 'DEBUG',
        'error_log': '/dev/null',
        'logger_name': 'db_daemon'
    },
    'server': {
        'commit_log': '/etc/dbdaemon_commit.log',
    },
    'replication': {
        'server_host': '127.0.0.1',
        'server_port': '7001',
        'replica_connect_timeout': 15,
        'config_path': '/etc/dbdaemon_replica.conf',
        'down_timeout': 15,
        'priority': 1,
    }
}