import logging
import os
import traceback

from queue import Queue, Empty, Full
from time import time, sleep
from threading import Lock

from .config import settings
from .list_ordered_dict import ListOrderedDict
from .reverse_read import reverse_read
from .stop_thread import StopThread

SEPARATOR = b'||'
U_SEPARATOR = SEPARATOR.decode()
EOL = b'\r\n'


class _CommitLogThread(StopThread):
    _queue = None
    _last_write_timestamp = 0
    __logger = None
    cluster = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lock = Lock()
        self.cache = ListOrderedDict()
        self._queue = Queue()

    @property
    def logger(self):
        if not self.__logger:
            self.__logger = logging.getLogger(settings.get('logger_name'))
        return self.__logger

    @property
    def commit_log(self):
        return settings.get('commit_log', section='server')

    def run(self):
        offset_error = False
        lqueue = self._queue
        with CommitLog(self.commit_log, 'ab', buffering=0) as f:
            while True:
                try:
                    item = lqueue.get(block=True, timeout=1)
                    if isinstance(item, list):
                        try:
                            ts, offset = item[0].split('||')
                            if int(offset) != f.tell() and not offset_error:
                                offset_error = True
                                print('PROTOCOL ERROR, line offset: {}, '
                                      'file offset: {}'.format(offset, f.tell()))
                            f.write(item[1], ts)
                        except Exception as e:
                            print('COMMIT LOG ERROR')
                            print(str(e))
                        continue
                    self.cluster.last_commit_key = f.write(item)
                except Empty:
                    if not self._should_run:
                        break
        self.logger.info('Termination commit log thread')

    def write(self, data):
        while True:
            try:
                self._queue.put(data, block=True, timeout=1)
                break
            except Full:
                self.logger.warning('Queue is full for more than one second')


class ReadCommitLogThread(StopThread):
    _init_done = False
    cache_size = 1000  # items
    chunk_size = 100000  # bytes

    cache = None
    lock = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.lock = Lock()
        self.init_lock = Lock()
        self.cache = ListOrderedDict()
        self.checkpoints = []
        self.commit_file = settings.get('commit_log', section='server')
        self.commit_log = CommitLog(self.commit_file, 'rb')

    def read(self, after_key: str=None, lines=1):
        while True:
            done = True
            with self.init_lock:
                if not self._init_done:
                    done = False
            if not done:
                sleep(0.2)
            else:
                break

        # cache hit
        with self.lock:
            if after_key in self.cache:
                i = 0
                data = []
                key = after_key

                while i < lines:
                    try:
                        key = self.cache.next_key(prev_key=key)
                        data.append((key, self.cache[key]))
                        i += 1
                    except ValueError:
                        break
                return data
        # go to disk
        with CommitLog(self.commit_file, 'rb') as f:
            current_offset = 0
            first_loop = False
            if after_key:
                current_offset = int(after_key.split(U_SEPARATOR, 1)[1])
                first_loop = True
            f.seek(current_offset)
            lines_read = 0
            data = []
            raw_data = b''
            while True:
                raw_data += f.read(self.chunk_size)
                if not raw_data:
                    print('No more data found. len: {}'.format(len(data)))
                    return data
                data_lines = raw_data.split(EOL)
                raw_data = data_lines.pop(-1)
                if first_loop:
                    l = data_lines.pop(0)
                    current_offset += len(EOL) + len(l)
                    first_loop = False
                for line in data_lines:
                    try:
                        ts, line_data = line.split(SEPARATOR, 1)
                        key = ts.decode() + U_SEPARATOR + str(current_offset)
                        data.append((key, line_data))
                        lines_read += 1
                        current_offset += len(line) + len(EOL)
                        if lines_read >= lines:
                            return data
                    except Exception as e:
                        self.logger.critical('EXCEPTION ON READ COMMIT LOG')
                        self.logger.critical(line)
                        self.logger.critical(str(e))
                        self.logger.critical(traceback.format_exc())

    def run(self):
        self._init_cache()
        self.commit_log.seek(0, os.SEEK_END)
        data = b''
        offset = self.commit_log.tell()
        while self._should_run:
            new_data = self.commit_log.read(self.chunk_size)
            data += new_data
            if not new_data:
                sleep(0.2)
            lines = data.split(EOL)
            data = lines.pop(-1)
            for i, line in enumerate(lines):
                self._add_line_to_cache(*line.split(SEPARATOR, 1), line_offset=offset)
                offset += len(EOL) + len(line)

    @property
    def last_disk_key(self):
        try:
            offset, line = reverse_read(self.commit_file, line_ending=EOL).__next__()
            ts, _ = line.split(SEPARATOR, 1)
            return ts.decode() + U_SEPARATOR + str(offset)
        except Exception as e:
            print(str(e))
            return None

    def _init_cache(self):
        lines_read = 0
        for offset, line in reverse_read(self.commit_file, line_ending=EOL):
            if lines_read >= self.cache_size:
                break
            self._add_line_to_cache(*line.split(SEPARATOR, 1), line_offset=offset)
            lines_read += 1
        cache = self.cache
        self.cache = ListOrderedDict()
        for k in reversed(cache):
            self.cache[k] = cache[k]
        with self.init_lock:
            self._init_done = True

    def _add_line_to_cache(self, ts: bytes, data: bytes, line_offset: int):
        with self.lock:
            self.cache[ts.decode() + U_SEPARATOR + str(line_offset)] = data
            if len(self.cache) > self.cache_size:
                self.cache.pop(self.cache.first_key(), None)


class CommitLog:
    _file = None

    def __init__(self, file, *args, **kwargs):
        self._file = open(file, *args, **kwargs)

    def seek(self, offset, from_what=0):
        return self._file.seek(offset, from_what)

    def write(self, data: bytes, ts: str=None) -> str:
        if not ts:
            ts = str(time())
        offset = self._file.tell()
        self._file.write(ts.encode() + SEPARATOR + data + EOL)
        return ts + U_SEPARATOR + str(offset)

    def read(self, n: int=None):
        return self._file.read(n)

    def tell(self):
        return self._file.tell()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._file.close()


commit_log = _CommitLogThread()
