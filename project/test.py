from dbdaemon.src.list_ordered_dict import ListOrderedDict
from client import Connection
#
c = Connection('127.0.0.1', 7000, 30)
# coll = c.select_database('test_db').select_collection('test')
# for i in range(2500):
#     if i % 1000 == 0:
#         print('{} inserted'.format(i))
#     coll.insert({'a': i})

# a = ListOrderedDict(a='b')
# a['b'] = 'c'
# print(a.first_key())
# a.next_key('b')
# a.pop('a')
# print(a.first_key())
# #
print('=========== START ===================')
print(c.set_as_master('192.168.56.101:6999'))
print('=========== END ======================')
print('=========== START ===================')
print(c.add_slave_to_cluster('192.168.56.102:6999'))
print(c.add_slave_to_cluster('192.168.56.103:6999'))
print('=========== END ======================')
print('=========== START ===================')
print(c.cluster_status())
print('=========== END ======================')
