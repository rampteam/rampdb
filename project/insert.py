from time import time
from client import Connection
#
c = Connection('127.0.0.1', 7000, 30)
coll = c.select_database('test_db').select_collection('test')
start = time()
for i in range(2500):
    if i % 1000 == 0:
        print('{} inserted'.format(i))
    coll.insert({'a': i})

print('FINISHED IS {} seconds'.format(time() - start))


#
# def threaded_function(start):
#     c = Connection('127.0.0.1', 7000, 30)
#     coll = c.select_database('test_db').select_collection('test')
#     for i in range(start, start + 1000):
#         coll.insert({'a': i})
#
#
# thread1 = Thread(target=threaded_function, args=(0, ))
# thread2 = Thread(target=threaded_function, args=(1000, ))
# thread3 = Thread(target=threaded_function, args=(2000, ))
#
# start = time()
#
# thread1.start()
# thread2.start()
# thread3.start()
#
# thread1.join()
# thread2.join()
# thread3.join()
